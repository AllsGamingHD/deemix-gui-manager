<?php

namespace App\Manager;

use App\Entity\Artist;
use App\Entity\Parameters;
use App\Helper\NotifierHelper;
use App\Provider\DeezerApiProvider;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;

class RefreshManager
{
    private ?EntityManagerInterface $entityManager;
    private ?ImportationManager $importationManager;
    private ?NotifierHelper $notifierHelper;


    public function __construct(EntityManagerInterface $entityManager, ImportationManager $importationManager, NotifierHelper $notifierHelper)
    {
        $this->entityManager      = $entityManager;
        $this->importationManager = $importationManager;
        $this->notifierHelper     = $notifierHelper;
    }

    /**
     * @throws \JsonException
     */
    public function refreshArtist(): void
    {
        $this->notifierHelper->setContent('Refreshing', "Getting all artists")->notify();

        $artists = $this->entityManager->getRepository(Artist::class)->getNotMonitoredActive();
        if (count($artists) === 0)
        {
            $this->notifierHelper->setContent('No artist monitored', 'No refresh because no artist have monitor active!')->notify();
        }
        else
        {
            $importedAlbumCount    = 0;
            $artistWithNewReleased = [];
            foreach ($artists as $artist)
            {
                $this->notifierHelper->setAnimate(false)->setShowConfirmButton(false)
                                     ->setContent('Refreshing', "Check new released for {$artist->getName()}")->notify();


                $albums             = $this->importationManager->importAlbumsOfArtist($artist);
                $importedAlbumCount += count($albums);
                if (count($albums) > 0) $artistWithNewReleased[] = $artist;
            }
            $artistWithNewReleased = implode(', ', array_map(static function ($data)
            {
                return $data->getName();
            }, $artistWithNewReleased));
            $this->entityManager->getRepository(Parameters::class)->updateLastRefreshToNow();
            $this->entityManager->flush();
            $this->notifierHelper->setAnimate(false)->setShowConfirmButton(false)
                                 ->setContent('Refreshing', $importedAlbumCount > 0 ? "Found $importedAlbumCount new released (for : $artistWithNewReleased)" : "Any artist have new released")
                                 ->notify();
        }
    }
}