<?php

namespace App\Manager;

use App\Controller\ArtistCrudController;
use App\Entity\Album;
use App\Entity\Artist;
use App\Entity\AudioBitrate;
use App\Entity\Parameters;
use App\Entity\RecordType;
use App\Entity\Track;
use App\Helper\GuiUpdater;
use App\Helper\NotifierHelper;
use App\Provider\DeezerApiProvider;
use Doctrine\ORM\EntityManagerInterface;
use JetBrains\PhpStorm\ArrayShape;
use Psr\Log\LoggerInterface;

class ImportationManager
{
    private ?EntityManagerInterface $entityManager;
    private ?DeezerApiProvider $deezerApi;
    private ?NotifierHelper $notifierHelper;
    private ?LoggerInterface $logger;
    private ?GuiUpdater $guiUpdater;

    public function __construct(EntityManagerInterface $entityManager, DeezerApiProvider $deezerApi, NotifierHelper $notifierHelper, LoggerInterface $logger, GuiUpdater $guiUpdater)
    {
        ini_set('memory_limit', '1G');
        $this->entityManager  = $entityManager;
        $this->deezerApi      = $deezerApi;
        $this->notifierHelper = $notifierHelper;
        $this->logger         = $logger;
        $this->guiUpdater     = $guiUpdater;
    }


    private ?Parameters $parameters;
    private ?array $artists;
    private ?array $existingArtists;
    private ?array $artistsToAdd;
    private ?array $artistsAlreadyExisting;

    /**
     * @param array $artist
     */
    private function pushArtistId(array $artist): void
    {
        $recordType = null;
        $bitrate    = null;
        if (isset($artist['recordType']))
        {
            $recordType = $this->entityManager->getRepository(RecordType::class)->findOneBy(['type' => $artist['recordType']]);
        }
        if (isset($artist['bitrate']))
        {
            $bitrate = $this->entityManager->getRepository(AudioBitrate::class)->findOneBy(['bitrateNumber' => $artist['bitrate']]);
        }

        $downloadPath                       = $data['downloadPath'] ?? $this->parameters->getDownloadPath();
        $alert                              = $data['alert'] ?? $this->parameters->getAlerting();
        $recordType                         = $recordType ?? $this->parameters->getRecordType();
        $bitrate                            = $bitrate ?? $this->parameters->getBitrate();
        $this->artists[$artist['artistId']] = [
            'downloadPath' => $downloadPath,
            'alert'        => $alert,
            'recordType'   => $recordType,
            'bitrate'      => $bitrate,
        ];
    }

    private function manageData(array $artists): void
    {
        $this->parameters   = $this->entityManager->getRepository(Parameters::class)->findLast();
        $this->artistsToAdd = $this->artistsAlreadyExisting = $this->artists = $this->existingArtists = null;
        if (count($artists) === count($artists, COUNT_RECURSIVE))
        {
            $this->pushArtistId($artists);
        }
        else
        {

            foreach ($artists as $artist)
            {
                $this->pushArtistId($artist);
            }
        }

        $this->existingArtists = $this->entityManager->getRepository(Artist::class)->findAll();
        $existingArtistsId     = array_map(static function ($data)
        {
            return $data->getArtistId();
        }, $this->existingArtists);

        $this->artistsToAdd           = array_filter($this->artists, static function ($artistId) use ($existingArtistsId)
        {
            if (!in_array($artistId, $existingArtistsId))
            {
                return $artistId;
            }
        },                                           ARRAY_FILTER_USE_KEY);
        $this->artistsAlreadyExisting = array_diff_key($this->artists, $this->artistsToAdd);

        $artistsCount           = count($this->artists);
        $artistsToAddCount      = count($this->artistsToAdd);
        $artistsAlreadyExisting = count($this->artistsAlreadyExisting);
        $this->notifierHelper->setTitle('Import Information')->setIcon('info')
                             ->setText("You request to import $artistsCount (We import $artistsToAddCount because $artistsAlreadyExisting artists already present)")
                             ->notify();
    }

    /**
     * @param array $artists ['artistId', 'downloadPath', 'recordType', 'bitrate']
     *
     * @throws \JsonExceptionSoprano
     */
    public function importArtist(array $artists): void
    {
        $this->logger->info('[ARTIST IMPORT] -- Managing Data');
        $this->manageData($artists);
        $this->logger->info('[ARTIST IMPORT] -- Successfully managed data !');

        $this->notifierHelper->setIcon('info')->setShowConfirmButton(false)->setAnimate(false)->setContent('Import artist', "Start task")->notify();
        $addedArtist = [];
        foreach ($this->artistsToAdd as $artistId => $artistConfiguration)
        {
            $artist = $this->importOneArtist($artistId, $artistConfiguration);
            if ($artist)
            {
                $addedArtist[$artist->getArtistId()] = $artist;
                $albums                              = $this->importAlbumsOfArtist($artist);
                if (!$this->entityManager->isOpen()) $this->entityManager->getConnection();
                $this->parameters->setLastImport(new \DateTime());
                $this->entityManager->flush();
                $this->guiUpdater->update(ArtistCrudController::class, 'create', $artist->serialize());
            }
        }
        $this->notifierHelper->setIcon('success')->setShowConfirmButton(true)->setAnimate(true)->setContent('Import artist', "Finished import task")
                             ->notify();
    }

    /**
     * Import one artist
     *
     * @param int   $artistId
     * @param array $artistConfiguration
     *
     * @return Artist|null
     * @throws \JsonException
     */
    private function importOneArtist(int $artistId, array $artistConfiguration): ?Artist
    {
        $dzArtistInfo = $this->deezerApi->getArtistById($artistId);
        if ($dzArtistInfo['nb_album'] > 0)
        {
            if ($this->parameters->getDeepDataGrap()->getId() !== 1)
            {
                $this->notifierHelper->setContent('Import artist', "Importing artist [{$dzArtistInfo['name']}]")->notify();
            }
            $artist = new Artist();
            $artist->setArtistId($dzArtistInfo['id'])
                   ->setName($dzArtistInfo['name'])
                   ->setUrl($dzArtistInfo['link'])
                   ->setPictureUrl($dzArtistInfo['picture_xl'])
                   ->setNbAlbums($dzArtistInfo['nb_album'])
                   ->setAlert($artistConfiguration['alert'])
                   ->setDownloadPath($artistConfiguration['downloadPath'])
                   ->setBitrate($artistConfiguration['bitrate'])
                   ->setRecordType($artistConfiguration['recordType'])
                   ->setImport($this->parameters->getDeepDataGrap());
            if (!$this->entityManager->isOpen()) $this->entityManager->getConnection();
            $this->entityManager->persist($artist);
            return $artist;
        }
        return null;
    }

    /**
     * @param Artist $artist
     *
     * @return Album[]
     * @throws \JsonException
     */
    public function importAlbumsOfArtist(Artist $artist): array
    {
        $this->logger->info("[IMPORT ARTIST] -- Get existing albums of [{$artist->getName()}]");


        $dzAlbums    = $this->deezerApi->getArtistAlbumsById($artist->getArtistId());
        $dzAlbumsIds = [];
        foreach ($dzAlbums as $alb)
        {
            $dzAlbumsIds[$alb['id']] = $alb;
        }
        $currentExistingAlbums    = $this->entityManager->getRepository(Album::class)->getAlbumsExisting(array_keys($dzAlbumsIds));
        $currentExistingAlbumsIds = array_map(static function ($album)
        {
            return $album['albumId'];
        }, $currentExistingAlbums);
        $this->logger->info("Get API albums of [{$artist->getName()}]");

        $this->logger->info("Check release date limit");
        if ($dzAlbums && $this->parameters->getCheckReleaseByDate())
        {
            $dzAlbums = $this->excludeAlbumWhereNotOutOfRange($dzAlbums, $this->parameters->getCheckReleaseByDateMaxDays());
        }

        $dzAlbums = $this->filterAlbumsKeepOnlyNotMonitored($dzAlbums, $currentExistingAlbumsIds);
        $dzAlbums = $this->getOnlyAlbumOfRecordType($dzAlbums, $artist->getArtistImportConfiguration()['record_type']->getType());


        $addedAlbums = [];
        $totalAlbums = count($dzAlbums);
        $progress    = 0;
        $this->logger->info("Importing albums of [{$artist->getName()}]");
        if ($this->parameters->getDeepDataGrap()->getId() !== 1)
        {
            $this->notifierHelper->setContent('Import artist', "Importing albums of [{$artist->getName()}]", "$progress/$totalAlbums")->notify();
        }
        foreach ($dzAlbums as $dzAlbumInfo)
        {
            $progress++;
            if ($this->parameters->getDeepDataGrap()->getId() !== 1)
            {
                $this->notifierHelper->setContent('Import artist', "Import album [{$dzAlbumInfo['title']}] of {$artist->getName()}", "$progress/$totalAlbums")
                                     ->notify();
            }
            if ($this->parameters->getDeepDataGrap()->getId() === 3)
            {
                $dzAlbumInfo = $this->deezerApi->getAlbumById($dzAlbumInfo['id']);
            }
            $album = $this->importOneAlbum($dzAlbumInfo, $artist);
            if ($this->parameters->getDeepDataGrap()->getId() === 2 || $this->parameters->getDeepDataGrap()->getId() === 3)
            {
                $albumsTracks[$album->getAlbumId()] = $this->importTracksOfAlbum($album);
            }
            $addedAlbums[$album->getAlbumId()] = $album;
        }
        return $addedAlbums;
    }

    private function importOneAlbum(array $dzAlbumInfo, Artist $artist): Album
    {
        $rType = $this->entityManager->getRepository(RecordType::class)->findOneBy(['type' => $dzAlbumInfo['record_type']]);
        $album = new Album();
        $album->setAlbumId($dzAlbumInfo['id'])
              ->setName($dzAlbumInfo['title'])
              ->setUpc($dzAlbumInfo['upc'] ?? null)
              ->setUrl($dzAlbumInfo['link'])
              ->setCoverUrl($dzAlbumInfo['cover_xl'] ?? 'https://img.lovepik.com/element/45000/9318.png_300.png')
              ->setGenres(array_column($dzAlbumInfo['genres']['data'] ?? ['name' => null], 'name'))
              ->setLabel($dzAlbumInfo['label'] ?? null)
              ->setNbTracks($dzAlbumInfo['nb_tracks'] ?? null)
              ->setDuration($dzAlbumInfo['duration'] ?? null)
              ->setNbFans($dzAlbumInfo['fans'])
              ->setRating($dzAlbumInfo['rating'] ?? null)
              ->setReleasedAt(new \DateTimeImmutable($dzAlbumInfo['release_date']))
              ->setBitrate($artist->getArtistImportConfiguration()['bitrate'])
              ->setRecordType($rType)
              ->setIsAvailable($dzAlbumInfo['available'] ?? null)
              ->setIsExplicit($dzAlbumInfo['explicit_lyrics'])
              ->setIsExplicitContent($dzAlbumInfo['explicit_content_lyrics'] ?? null)
              ->setIsExplicitCover($dzAlbumInfo['explicit_content_cover'] ?? null)
              ->setContributors(array_column($dzAlbumInfo['contributors'] ?? ['name' => null], 'name') ?? [])
              ->setArtist($artist);
        if (!$this->entityManager->isOpen()) $this->entityManager->getConnection();
        $this->entityManager->persist($album);

        return $album;
    }

    /**
     * @param Album $album
     *
     * @return Track[]
     * @throws \JsonException
     */
    private function importTracksOfAlbum(Album $album): array
    {
        $this->logger->info("Getting existing all existing tracks");
        $dzAlbumTracklist = $this->deezerApi->getAlbumTracks($album->getAlbumId());
        $albumTracks      = [];
        foreach ($dzAlbumTracklist as $track)
        {
            $albumTracks[$track['id']] = $track;
        }
        $this->logger->info("Keep only not imported tracks");
        $alreadyExistingTracks = $this->entityManager->getRepository(Track::class)->getTracksExisting(array_keys($albumTracks));

        if (count($alreadyExistingTracks) > 0)
        {
            $alreadyExistingTracks = array_column($alreadyExistingTracks, 'trackId');
            $dzAlbumTracklist      = array_map(static function ($trackId) use ($albumTracks)
            {
                return $albumTracks[$trackId];
            }, $alreadyExistingTracks);
        }

        $addedTracks = [];
        foreach ($dzAlbumTracklist as $dzTrack)
        {
            if ($this->parameters->getDeepDataGrap()->getId() === 3)
            {
                $dzTrack = $this->deezerApi->getTrackById($dzTrack['id']);
            }
            if (!array_key_exists($dzTrack['id'], $alreadyExistingTracks))
            {
                $track = $this->importOneTrack($dzTrack, $album);
                $album->addTrack($track);
            }
            else
            {
                $track = $alreadyExistingTracks[$dzTrack['id']];
                if (!$album->getTracks()->contains($track))
                {
                    $album->addTrack($track);
                }
            }
            $addedTracks[$album->getAlbumId()][$track->getTrackId()] = $track;
        }
        return $addedTracks;
    }

    /**
     * @param array $dzTrackInfo
     * @param Album $album
     *
     * @return Track
     * @throws \Exception
     */
    private function importOneTrack(array $dzTrackInfo, Album $album): Track
    {
        $this->logger->info("[IMPORT ARTIST] -- Adding track [{$dzTrackInfo['title']} from album [{$album->getName()}] by {$album->getArtist()->getName()}]");
        $track = new Track();
        $track->setTrackId($dzTrackInfo['id'])
              ->setTitle($dzTrackInfo['title'])
              ->setTitleShort($dzTrackInfo['title_short'] ?? null)
              ->setTitleVersion($dzTrackInfo['title_version'] ?? null)
              ->setUrl($dzTrackInfo['link'])
              ->setDuration($dzTrackInfo['duration'])
              ->setRank($dzTrackInfo['rank'] ?? null)
              ->setIsExplicitLyrics($dzTrackInfo['explicit_lyrics'])
              ->setIsrc($dzTrackInfo['isrc'])
              ->setTrackPosition($dzTrackInfo['track_position'])
              ->setDiskNumber($dzTrackInfo['disk_number'])
              ->setReleasedAt(isset($dzTrackInfo['release_date']) ? new \DateTimeImmutable($dzTrackInfo['release_date']) : $album->getReleasedAt())
              ->setBpm($dzTrackInfo['bpm'] ?? null)
              ->setGain($dzTrackInfo['gain'] ?? null)
              ->setAvailableIn($dzTrackInfo['available_countries'] ?? null)
              ->setContributors(array_column($dzTrackInfo['contributors'] ?? ['name' => null], 'name'))
              ->setPreviewUrl($dzTrackInfo['preview'])
              ->setArtist($album->getArtist());
        if (!$this->entityManager->isOpen()) $this->entityManager->getConnection();
        $this->entityManager->persist($track);
        return $track;
    }

    /**
     * Return only album of selected/default configuration record type
     *
     * @param array       $albums     Array containing all albums to filter
     * @param string|null $recordType Filter type to keep
     *
     * @return array
     */
    private function getOnlyAlbumOfRecordType(array $albums, ?string $recordType): array
    {
        $this->logger->info("Filter albums by only one record type");
        return array_filter($albums, static function ($data) use ($recordType)
        {
            if ($recordType === $data['record_type'] || $recordType === 'all')
            {
                return $data;
            }
        });
    }

    /**
     * Return array of albums not already monitored
     *
     * @param array $dzArtistAlbums   albums coming from deezer API
     * @param array $existingAlbumsId albums ID's already monitored
     *
     * @return array
     */
    private function filterAlbumsKeepOnlyNotMonitored(array $dzArtistAlbums, array $existingAlbumsId): array
    {
        $this->logger->info("Filter albums and keep only not monitored");
        return array_filter($dzArtistAlbums, static function ($dzAlbum) use ($existingAlbumsId)
        {
            if (!in_array($dzAlbum['id'], $existingAlbumsId))
            {
                return $dzAlbum;
            }
        });
    }

    private function excludeAlbumWhereNotOutOfRange(array $albums, \DateTime $dateTimeLimit): array
    {
        $this->logger->info("Filter albums and keep only where release date not out of range");
        return array_filter($albums, static function ($album) use ($dateTimeLimit)
        {
            if (new \DateTime($album['release_date']) > $dateTimeLimit)
            {
                return $album;
            }
        });
    }

    /**
     * Import artist of each track in monitoring list;
     *
     * @param int $playlistId
     *
     * @throws \JsonException
     */
    public function importArtistFromPlaylist(int $playlistId, string $opt = 'mainArtistOnly')
    {
        $dzPlaylistTracksInfo = $this->deezerApi->getPlaylistTracklistById($playlistId);
        if ($dzPlaylistTracksInfo)
        {
            $this->notifierHelper->setContent('Importing artists from playlist', 'Starting task');
            $dzPlaylistArtists = [];
            if ($opt === 'mainArtistOnly')
            {
                $this->logger->info("Getting artists of playlist");
                $dzPlaylistArtists = array_unique(array_column(array_column($dzPlaylistTracksInfo, 'artist'), 'id'), SORT_NUMERIC);
                $dzPlaylistArtists = array_map(static function ($data)
                {
                    return ['artistId' => $data];
                }, $dzPlaylistArtists);
            }
            else if ($opt === 'artistAndContributors')
            {
                $this->logger->info("Getting artists and contributors of playlist");
                $dzPlaylistArtists = [];
                foreach ($dzPlaylistTracksInfo as $track)
                {
                    $dzTrackInfo    = $this->deezerApi->getTrackById($track['id']);
                    $trackArtistsId = array_column($dzTrackInfo['contributors'], 'id');
                    foreach ($trackArtistsId as $artistId)
                    {
                        if (!in_array($artistId, $dzPlaylistArtists))
                        {
                            $dzPlaylistArtists[] = ['artistId' => $artistId];
                        }
                    }
                }
            }
            $this->importArtist($dzPlaylistArtists);
            $this->notifierHelper->setContent('Importing artists from playlist', 'Task ended successfully');
        }
        else
        {
            $this->notifierHelper->setIcon('danger')->setContent('Importing artists from playlist', 'No tracks found in this playlist :-(');
        }
    }
}