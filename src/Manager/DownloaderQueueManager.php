<?php

namespace App\Manager;

use App\Controller\AlbumCrudController;
use App\Controller\ArtistCrudController;
use App\Entity\Album;
use App\Helper\GuiUpdater;
use App\Helper\NotifierHelper;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Process\Process;

class DownloaderQueueManager
{
    private const MAX_INSTANCE = 200;
    private array $queue = [];
    private int $progress = 0;
    private array $processingQueue = [];
    private int $total = 0;
    private array $retries = [];

    public function __construct(EntityManagerInterface $entityManager, NotifierHelper $notifierHelper, LoggerInterface $logger, GuiUpdater $guiUpdater)
    {
        $this->entityManager  = $entityManager;
        $this->notifierHelper = $notifierHelper;
        $this->logger         = $logger;
        $this->guiUpdater     = $guiUpdater;
    }

    public function addToQueue(Album $album)
    {
        $this->total++;
        $this->queue[] = $album;
    }

    public function startQueue()
    {
        while (count($this->queue) !== 0 || count($this->processingQueue) !== 0)
        {
            $inDownload  = count($this->processingQueue);
            $this->state = "Downloading state [$inDownload] -- {$this->progress}/{$this->total}";
            if (count($this->processingQueue) < self::MAX_INSTANCE && count($this->queue) !== 0)
            {
                $serialize = [
                    'id'           => $this->queue[$this->progress]->getId(),
                    'albumId'      => $this->queue[$this->progress]->getAlbumId(),
                    'isDownloaded' => 1,
                ];
                $this->guiUpdater->update(AlbumCrudController::class, 'update', $serialize);
                $this->startProcessInstance($this->queue[$this->progress]);
                unset($this->queue[$this->progress++]);
            }
            else
            {
                foreach ($this->processingQueue as $key => $queueInfo)
                {
                    $process = $queueInfo['process'];
                    $album   = $queueInfo['album'];
                    if (!$process->isRunning())
                    {
                        if (trim(substr($process->getOutput(), -11, 11)) === 'All done!')
                        {
                            $this->logger->notice("{$this->state} | Successfully downloaded [{$album->getAlbumId()}] {$album->getName()}");
                            unset($this->processingQueue[$key]);
                            $inDownload  = count($this->processingQueue);
                            $this->state = "Downloading state [$inDownload] -- {$this->progress}/{$this->total}";
                            $this->entityManager->getRepository(Album::class)->setIsDownloadedById($album->getAlbumId());
                            $isFullyDownloaded = $this->entityManager->getRepository(Album::class)
                                                                     ->updateArtistIfAllDownloaded($album->getArtist()->getArtistId());
                        }
                        else
                        {
                            if (!isset($this->retries[$album->getId()]))
                            {
                                $this->retries[$album->getId()] = 1;
                            }
                            else if ($this->retries[$album->getId()] >= 3)
                            {
                                $process->stop();
                                unset($this->retries[$album->getId()], $this->processingQueue[$key]);
                                $this->entityManager->getRepository(Album::class)->setDownloadedFailedById($album->getAlbumId());
                            }
                            else
                            {
                                ++$this->retries[$album->getId()];
                            }

                            $msg = $this->retries[$album->getId()] ?? 'CANNOT DOWNLOAD';
                            $this->logger->alert("{$this->state} | FAILED ATTEMPT {$msg}, Restart download [{$album->getAlbumId()}] {$album->getName()}");
                            if (isset($this->retries[$album->getId()])) $process->restart();
                        }
                    }
                }
                sleep(1);
            }
        }
    }

    private function startProcessInstance(Album $album)
    {
        $serialize = [
            'id'           => $album->getId(),
            'albumId'      => $album->getAlbumId(),
            'isDownloaded' => 1,
        ];
        /* $this->guiUpdater->update(ArtistCrudController::class, 'updateAlb', $serialize);
         $this->guiUpdater->update(AlbumCrudController::class, 'update', $serialize);
         $this->entityManager->getRepository(Album::class)->setIsDownloadingById($album->getAlbumId());
         $this->notifierHelper->setContent('Start downloading : ' . $album->getName() . '.', count($this->processingQueue))->notify();*/
        $inDownload  = count($this->processingQueue);
        $this->state = "Downloading state [$inDownload] -- {$this->progress}/{$this->total}";
        $this->logger->info("{$this->state} | Start downloading album [{$album->getAlbumId()}] {$album->getName()}");
        $process = Process::fromShellCommandline("py -m deemix -b {$album->getArtist()->getBitrate()->getBitrateNumber()} -p {$album->getArtist()->getDownloadPath()} {$album->getUrl()}");
        $process->setTimeout(600);
        $process->setIdleTimeout(600);
        $process->start();
        $this->processingQueue[$album->getId()] = ['album' => $album, 'process' => $process];
    }
}