<?php

namespace App\Adapters;

interface ImporterInterface
{
    public function import(string $filePath): array;
}