<?php

namespace App\Form;

use App\Entity\AudioBitrate;
use App\Entity\Parameters;
use App\Entity\RecordType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotNull;

class ImportConfigurationType extends AbstractType
{
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $parameters = $this->entityManager->getRepository(Parameters::class)->findLast();
        $builder
            ->add('downloadPath', TextType::class, [
                'help'        => 'Where artists musics will be downloaded',
                'empty_data'  => $parameters->getDownloadPath(),
                'data'        => $parameters->getDownloadPath(),
                'constraints' => [
                    new NotNull(),
                ],
            ])
            ->add('alert', CheckboxType::class, [
                'row_attr' => ['class' => 'form-switch'],
                'help'     => 'If checked, you will receive an email alert when artist have new release. (need to configure email alerting)',
                'required' => false,
                'empty_data'     => $parameters->getAlerting() ? true : null,
                'data'     => $parameters->getAlerting() ? true : null,
            ])
            ->add('bitrate', ChoiceType::class, [
                'help'    => 'Audio quality for this artist',
                'choices' => array_flip(AudioBitrate::AUDIO_BITRATE),
                'empty_data'    => (string)$parameters->getBitrate()->getBitrateNumber(),
                'data'    => (string)$parameters->getBitrate()->getBitrateNumber(),
            ])
            ->add('recordType', ChoiceType::class, [
                'help'    => 'Type of record for this artist',
                'choices' => array_flip(RecordType::RECORD_TYPES),
                'empty_data'    => $parameters->getRecordType()->getType(),
                'data'    => $parameters->getRecordType()->getType(),
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
                                   "allow_extra_fields" => true,
                               ]);
    }
}