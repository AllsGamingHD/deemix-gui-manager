<?php

namespace App\Form;

use App\Entity\AudioBitrate;
use App\Entity\Parameters;
use App\Entity\RecordType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormError;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotNull;

class ImportFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('file', FileType::class, [
                'label' => 'File to import',
                'help'  => 'You can import file of type CSV, JSON or a Text File containing URL or ID on each line.',
            ])
            ->addEventListener(FormEvents::POST_SET_DATA, function (FormEvent $event)
            {
                $event->getForm()->add('submit', SubmitType::class, [
                    'attr'  => ['class' => 'btn btn-primary'],
                    'label' => 'Execute import',
                ]);
            })
            ->addEventListener(FormEvents::PRE_SUBMIT, function (FormEvent $event)
            {
                $file = $event->getData()['file'];
                $form = $event->getForm();
                if ($file->getClientMimeType() === 'text/plain')
                {
                    if (!isset($event->getData()['parameters']['downloadPath']))
                    {
                        $form->addError(new FormError('Detected you want to import a TXT file, we need some more information. Actually configuration is taken by your default parameters.'));
                    }
                    $form->add('parameters', ImportConfigurationType::class);
                    $form->remove('submit');
                    $form->add('submit_text', SubmitType::class, [
                        'attr'  => ['class' => 'btn btn-primary'],
                        'label' => 'Validate and execute import',
                    ]);
                }
            });

    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
                                   "allow_extra_fields" => true,
                               ]);
    }
}