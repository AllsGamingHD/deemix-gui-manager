<?php

namespace App\Helper;

use Psr\Log\LoggerInterface;
use Symfony\Component\Mercure\HubInterface;
use Symfony\Component\Mercure\Update;

class GuiUpdater
{
    private ?HubInterface $hub;

    public function __construct(HubInterface $hub)
    {
        $this->hub = $hub;
    }

    /**
     * @throws \JsonException
     */
    public function update(string $pageName, string $type, array $data): void
    {
        $res    = ['page' => $pageName, 'type' => $type, 'data' => $data];
        $update = new Update('http://localhost/gui', json_encode($res, JSON_THROW_ON_ERROR));
        $this->hub->publish($update);
    }
}