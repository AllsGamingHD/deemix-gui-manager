<?php

namespace App\Helper;

use Psr\Log\LoggerInterface;
use Symfony\Component\Mercure\HubInterface;
use Symfony\Component\Mercure\Update;

class NotifierHelper
{
    private ?HubInterface $hub;
    private ?LoggerInterface $logger;

    private bool $isToast = true;
    private string $position = 'bottom-start';
    private string $icon = 'success';
    private string $title = '';
    private string $text = '';
    private string $footer = '';
    private ?string $imageUrl = null;
    private int $imageHeight = 0;
    private string $imageAlt = '';
    private bool $showCloseButton = true;
    private bool $showCancelButton = false;
    private bool $showConfirmButton = true;
    private bool $showDenyButton = false;
    private string $closeButtonText = 'Close';
    private string $cancelButtonText = 'Cancel';
    private string $confirmButtonText = 'Confirm';
    private string $denyButtonText = 'Deny';
    private int $timerDuration = 5000;
    private bool $timerProgressBar = true;
    private array $cssShow;
    private array $cssHide;

    public function __construct(HubInterface $hub, LoggerInterface $logger)
    {
        $this->logger  = $logger;
        $this->hub     = $hub;
        $this->cssHide = [
            'popup'    => 'swal2-hide',
            'backdrop' => 'swal2-backdrop-hide',
            'icon'     => 'swal2-icon-hide',
        ];
        $this->cssShow = [
            'popup'    => 'swal2-show',
            'backdrop' => 'swal2-backdrop-show',
            'icon'     => 'swal2-icon-show',
        ];
    }

    public function setContent(string $title, string $text = '', string $footer = ''): NotifierHelper
    {
        $this->setTitle($title)->setText($text)->setFooter($footer);
        return $this;
    }

    /**
     * @param string $position = ['top', 'top-start', 'top-end', 'center', 'center-start', 'center-end', 'bottom', 'bottom-start', 'bottom-end']
     *
     * @return NotifierHelper
     */
    public function setPosition(string $position): NotifierHelper
    {
        $this->position = $position;
        return $this;
    }

    /**
     * @param string $icon = ['success', 'error', 'warning', 'info', 'question']
     *
     * @return NotifierHelper
     */
    public function setIcon(string $icon): NotifierHelper
    {
        $this->icon = $icon;
        return $this;
    }

    /**
     * @param string $title
     *
     * @return NotifierHelper
     */
    public function setTitle(string $title): NotifierHelper
    {
        $this->title = $title;
        return $this;
    }

    /**
     * @param string $text
     *
     * @return NotifierHelper
     */
    public function setText(string $text): NotifierHelper
    {
        $this->text = $text;
        return $this;
    }

    /**
     * @param string $footer
     *
     * @return NotifierHelper
     */
    public function setFooter(string $footer): NotifierHelper
    {
        $this->footer = $footer;
        return $this;
    }


    /**
     * @param string $imageUrl
     * @param int    $imageHeight
     * @param string $imageAlt
     *
     * @return NotifierHelper
     */
    public function setImage(string $imageUrl, int $imageHeight = 100, string $imageAlt = ''): NotifierHelper
    {
        $this->imageUrl    = $imageUrl;
        $this->imageHeight = $imageHeight;
        $this->imageAlt    = $imageAlt;
        return $this;
    }

    /**
     * @param bool $showCloseButton
     *
     * @return NotifierHelper
     */
    public function setShowCloseButton(bool $showCloseButton): NotifierHelper
    {
        $this->showCloseButton = $showCloseButton;
        return $this;
    }

    /**
     * @param bool $showCancelButton
     *
     * @return NotifierHelper
     */
    public function setShowCancelButton(bool $showCancelButton): NotifierHelper
    {
        $this->showCancelButton = $showCancelButton;
        return $this;
    }

    /**
     * @param bool $showConfirmButton
     *
     * @return NotifierHelper
     */
    public function setShowConfirmButton(bool $showConfirmButton): NotifierHelper
    {
        $this->showConfirmButton = $showConfirmButton;
        return $this;
    }

    /**
     * @param bool $showDenyButton
     *
     * @return NotifierHelper
     */
    public function setShowDenyButton(bool $showDenyButton): NotifierHelper
    {
        $this->showDenyButton = $showDenyButton;
        return $this;
    }

    /**
     * @param string $closeButtonText
     *
     * @return NotifierHelper
     */
    public function setCloseButtonText(string $closeButtonText): NotifierHelper
    {
        $this->closeButtonText = $closeButtonText;
        return $this;
    }

    /**
     * @param string $cancelButtonText
     *
     * @return NotifierHelper
     */
    public function setCancelButtonText(string $cancelButtonText): NotifierHelper
    {
        $this->cancelButtonText = $cancelButtonText;
        return $this;
    }

    /**
     * @param string $confirmButtonText
     *
     * @return NotifierHelper
     */
    public function setConfirmButtonText(string $confirmButtonText): NotifierHelper
    {
        $this->confirmButtonText = $confirmButtonText;
        return $this;
    }

    /**
     * @param string $denyButtonText
     *
     * @return NotifierHelper
     */
    public function setDenyButtonText(string $denyButtonText): NotifierHelper
    {
        $this->denyButtonText = $denyButtonText;
        return $this;
    }

    /**
     * @param int  $timerDuration
     * @param bool $showProgressBar
     *
     * @return NotifierHelper
     */
    public function setTimerDuration(int $timerDuration, bool $showProgressBar = true): NotifierHelper
    {
        $this->timerDuration    = $timerDuration;
        $this->timerProgressBar = $showProgressBar;
        return $this;
    }

    /**
     * @param bool $animate
     *
     * @return NotifierHelper
     * @throws \JsonException
     */
    public function setAnimate(bool $animate): NotifierHelper
    {
        $this->cssHide = [
            'popup'    => $animate ? 'swal2-hide' : '',
            'backdrop' => $animate ? 'swal2-backdrop-hide' : '',
            'icon'     => $animate ? 'swal2-icon-hide' : '',
        ];
        $this->cssShow = [
            'popup'    => $animate ? 'swal2-show' : '',
            'backdrop' => $animate ? 'swal2-backdrop-show' : '',
            'icon'     => $animate ? 'swal2-icon-show' : '',
        ];
        return $this;
    }

    /**
     * @param bool $isToast
     *
     * @return NotifierHelper
     */
    public function setIsToast(bool $isToast): NotifierHelper
    {
        $this->isToast = $isToast;
        return $this;
    }

    /**
     * @throws \JsonException
     */
    public function notify(): void
    {
        $data = [
            'toast'             => $this->isToast,
            'position'          => $this->position,
            'icon'              => $this->icon,
            'title'             => $this->title,
            'text'              => $this->text,
            'footer'            => $this->footer,
            'showCloseButton'   => $this->showCloseButton,
            'showCancelButton'  => $this->showCancelButton,
            'showConfirmButton' => $this->showConfirmButton,
            'showDenyButton'    => $this->showDenyButton,
            'confirmButtonText' => $this->confirmButtonText,
            'cancelButtonText'  => $this->cancelButtonText,
            'closeButtonText'   => $this->closeButtonText,
            'denyButtonText'    => $this->denyButtonText,
            'timer'             => $this->timerDuration,
            'timerProgressBar'  => $this->timerProgressBar,
            'showClass'         => $this->cssShow,
            'hideClass'         => $this->cssHide,
        ];
        if ($this->imageUrl)
        {
            $data['imageUrl']    = $this->imageUrl;
            $data['imageHeight'] = $this->imageHeight;
            $data['imageAlt']    = $this->imageAlt;
        }
        $this->logger->info("{$data['title']} -- {$data['text']}");
        $update = new Update('http://localhost/', json_encode($data, JSON_THROW_ON_ERROR));
        $this->hub->publish($update);
    }
}