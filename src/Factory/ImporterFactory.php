<?php

namespace App\Factory;

use App\Adapters\ImporterInterface;
use App\Importer\CsvImporter;
use App\Importer\JsonImporter;
use App\Importer\TextImporter;

class ImporterFactory
{
    public function getImporter(string $mimeType, array $options = []): ImporterInterface
    {
        return match ($mimeType)
        {
            'text/csv', 'application/csv', 'application/vnd.ms-excel' => new CsvImporter(options: $options),
            'application/json' => new JsonImporter(options: $options),
            'text/plain' => new TextImporter(options: $options),
            default => throw new \Exception('Import mime type isn\'t supported actually'),
        };
    }
}