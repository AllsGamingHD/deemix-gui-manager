<?php

namespace App\Importer;

use App\Adapters\ImporterInterface;

class TextImporter implements ImporterInterface
{
    private array $options = [];

    public function __construct(array $options = [])
    {
        $this->options = $options;
    }

    public function import(string $filePath): array
    {
        $file      = fopen($filePath, 'rb+');
        $finalData = [];
        while (($line = fgets($file)) !== false)
        {
            $finalData[]['artistId'] = trim($line);
        }

        fclose($file);
        return $finalData;
    }
}