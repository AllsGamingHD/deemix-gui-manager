<?php

namespace App\Importer;

use App\Adapters\ImporterInterface;
use App\Trait\ImporterTrait;

class JsonImporter implements ImporterInterface
{
    use ImporterTrait;

    public function __construct(array $options = [])
    {
        $this->options = $options;
    }

    /**
     * @throws \JsonException
     */
    public function import(string $filePath): array
    {
        $fileContent = file_get_contents($filePath);
        return $this->excludeColumns(json_decode($fileContent, true, 512, JSON_THROW_ON_ERROR));
    }
}