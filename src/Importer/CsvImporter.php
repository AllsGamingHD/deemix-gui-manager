<?php

namespace App\Importer;

use App\Adapters\ImporterInterface;
use App\Trait\ImporterTrait;

class CsvImporter implements ImporterInterface
{
    use ImporterTrait;

    private string $separator;
    private string $enclosure;
    private string $escape;
    private array $options = [];

    public function __construct(string $separator = ',', string $enclosure = '"', string $escape = '\\', array $options = [])
    {
        $this->separator = $separator;
        $this->enclosure = $enclosure;
        $this->escape    = $escape;
        $this->options   = $options;
    }

    public function import(string $filePath): array
    {
        $finalData = [];

        $file    = fopen($filePath, 'rb+');
        $columns = fgetcsv($file, 0, $this->separator, $this->enclosure, $this->escape);
        while (($data = fgetcsv($file, 0, $this->separator, $this->enclosure, $this->escape)) !== false)
        {
            $transformedArray = [];
            foreach ($data as $key => $value)
            {
                $transformedArray[$columns[$key]] = $value;
            }
            $finalData[] = $transformedArray;
        }
        fclose($file);
        return $this->excludeColumns($finalData);
    }
}