<?php

namespace App\Controller;

use App\Entity\Artist;
use App\Factory\ImporterFactory;
use App\Form\ImportFormType;
use App\Helper\NotifierHelper;
use App\Manager\ImportationManager;
use App\Message\ImportArtist;
use App\Message\RefreshArtist;
use App\Provider\DeezerApiProvider;
use Doctrine\ORM\EntityManagerInterface;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Context\AdminContext;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\BooleanField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateTimeField;
use EasyCorp\Bundle\EasyAdminBundle\Field\FormField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ImageField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IntegerField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Router\AdminUrlGenerator;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

class ArtistCrudController extends AbstractCrudController
{
    private ?TranslatorInterface $translator;
    private ?DeezerApiProvider $deezerApi;
    private ?EntityManagerInterface $entityManager;

    public function __construct(TranslatorInterface $translator, NotifierHelper $notifierHelper, DeezerApiProvider $deezerApi, EntityManagerInterface $entityManager)
    {
        $this->translator    = $translator;
        $this->deezerApi     = $deezerApi;
        $this->entityManager = $entityManager;
    }

    #[Route('/artist/add', name: 'artist_add_by_id', methods: ['POST'])]
    public function addArtistById(Request $request, NotifierHelper $notifierHelper, MessageBusInterface $bus): Response
    {
        if (!$request->request->has('artistId')) return new Response(json_encode(['status' => 'error', 'message' => 'Need parameter : "artistId"'], JSON_THROW_ON_ERROR), Response::HTTP_NO_CONTENT, ['Content-Type' => 'application/json']);
        $notifierHelper->setIcon('info')->setTitle('Starting task')->setText('Importing an artist')->notify();
        $bus->dispatch(new ImportArtist($request->request->all()));
        return new Response(json_encode(['status' => 'success', 'message' => 'Received call'], JSON_THROW_ON_ERROR), Response::HTTP_OK, ['Content-Type' => 'application/json']);
    }

    #[Route('/artist/refresh', name: 'artist_refresh', methods: ['GET'])]
    public function refreshArtist(MessageBusInterface $bus): Response
    {
        $bus->dispatch(new RefreshArtist());
        return new Response(json_encode(['status' => 'success', 'message' => 'Received call'], JSON_THROW_ON_ERROR), Response::HTTP_OK, ['Content-Type' => 'application/json']);
    }


    public function configureActions(Actions $actions): Actions
    {
        $import = Action::new('import', 'Import', 'fa fa-download')
                        ->linkToCrudAction('importArtist')
                        ->setCssClass('btn btn-success')
                        ->createAsGlobalAction();
        $export = Action::new('export', 'Export', 'fa fa-file-export')
                        ->linkToCrudAction('exportArtist')
                        ->setCssClass('btn btn-secondary')
                        ->createAsGlobalAction();
        return $actions->add(Crud::PAGE_INDEX, Action::DETAIL)
                       ->add(Crud::PAGE_INDEX, $import)
                       ->disable(Crud::PAGE_NEW)->add(Crud::PAGE_INDEX, $export);
    }

    public function exportArtist()
    {
        $artists = $this->entityManager->getRepository(Artist::class)->findAll();
        $f       = fopen('php://memory', 'rb+');
        // CSV
        /*$columns = ['id', 'record_type_id', 'bitrate_id', 'name', 'url', 'nb_albums', 'download_path', 'started_monitoring_at', 'alert', 'picture_url', 'is_fully_download', 'monitor_active', 'is_deep_import', 'added_at', 'download_on'];
        fputcsv($f, $columns);
        foreach ($artists as $artist)
        {
            fputcsv($f, [
                $artist->getId(), $artist->getRecordType()->getId(), $artist->getBitrate()
                                                                            ->getBitrateNumber(), $artist->getName(), $artist->getUrl(), $artist->getNbAlbums(), $artist->getDownloadPath(), $artist->getStartedMonitoringAt()
                                                                                                                                                                                                    ->format('Y-m-d H:i:s'), $artist->getAlert(), $artist->getPictureUrl(), $artist->getIsFullyDownload(), $artist->getMonitorActive(), $artist->getImport()
                                                                                                                                                                                                                                                                                                                                               ->getId(), $artist->getAddedAt()
                                                                                                                                                                                                                                                                                                                                                                 ->format('Y-m-d H:i:s'), $artist->getDownloadOn(),
            ]);
        }*/
        // TEXT
        /* foreach ($artists as $artist)
         {
             fwrite($f, $artist->getArtistId() . PHP_EOL);
         }*/
        // reset the file pointer to the start of the file
        fseek($f, 0);
        // tell the browser it's going to be a csv file
        header('Content-Type: text/plain');
        // tell the browser we want to save it instead of displaying it
        header('Content-Disposition: attachment; filename=artists-' . (new \DateTime())->format('Y-m-d H:i:s') . '.txt;');
        // make php send the generated csv lines to the browser
        fpassthru($f);
        return new Response('ok');
    }

    public function importArtist(AdminContext $adminContext, Request $request, ImportationManager $importationManager): Response
    {
        $form = $this->createForm(ImportFormType::class);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid())
        {
            $file = $form->getData()['file'];

            $importData = (new ImporterFactory())->getImporter($file->getClientMimeType(), [
                'excludedColumns' => ['started_monitoring_at'],
            ])->import($file->getRealPath());

            // TODO : Search why import not functional with messenger ? But work anywhere else except this function o.O
            $importationManager->importArtist($importData);

            $this->addFlash('success', 'Imported successfully');

            $routeBuilder = $this->get(AdminUrlGenerator::class);
            return $this->redirect($routeBuilder->setController(self::class)->setAction(Action::INDEX)->generateUrl());
        }

        return $this->render('admin/crud/artist/forms/import.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    public function configureCrud(Crud $crud): Crud
    {
        $artistCount = $this->entityManager->getRepository(Artist::class)->getTotalCountAndTotalDownloaded();
        $translator  = $this->translator;
        return $crud
            ->setPageTitle(Crud::PAGE_INDEX, $translator->trans('label.artists', [], 'messages') . " - ({$artistCount['totalDownloaded']}/{$artistCount['total']})")
            ->setPageTitle(Crud::PAGE_DETAIL, static function ($data) use ($translator)
            {
                return $translator->trans('label.artist.detail_title', ['%artist_name%' => $data->getName(), '%artist_id%' => $data->getArtistId()], 'messages');
            })
            ->overrideTemplate('crud/index', 'admin/crud/artist/index.html.twig')
            ->overrideTemplate('crud/detail', 'admin/crud/artist/detail.html.twig')
            ->setDateTimeFormat($this->get('session')->get('_locale') === 'fr' ? 'dd/MM/Y @ H:mm:ss' : 'Y-MM-dd @ H:mm:ss')
            ->setDateFormat($this->get('session')->get('_locale') === 'fr' ? 'dd/MM/Y' : 'Y-MM-dd');
    }

    public function configureFields(string $pageName): iterable
    {
        yield BooleanField::new('monitorActive', 'label.is_monitoring')
                          ->renderAsSwitch(true);
        yield BooleanField::new('isFullyDownload', 'label.is_fully_downloaded')->addCssClass('isFullyDownloaded')->renderAsSwitch(false)->hideOnForm()
                          ->setHelp('Indicate if artist is up to date and have no download left.');
        yield FormField::addPanel('label.artist_information');
        yield IntegerField::new('artistId', 'label.artist_id')->setHelp('Artist ID only (not url)')->hideOnForm();
        yield TextField::new('name', 'label.artist_name')->setHelp('Artist name (it will auto fetch if you set ID in above input)')
                       ->hideOnForm();
        yield TextField::new('url', 'label.url')->setHelp('URL To download or access artist page')->onlyOnDetail()
                       ->hideOnForm();
        yield ImageField::new('pictureUrl', 'label.artist_picture')->setHelp('Artist Cover')->onlyOnDetail();
        yield IntegerField::new('nbAlbums', 'label.total_albums')->setHelp('Number of albums released by artist')->hideOnForm();
        yield TextField::new('nbDownloadedAlbums', 'label.download_progress')->addCssClass('downloadProgress')->hideOnForm();
        yield TextField::new('downloadPath', 'label.download_path')->setHelp('Where artists musics will be downloaded');
        yield AssociationField::new('recordType', 'label.record_type')->setRequired(true)->setHelp('Type of record for this artist');
        yield AssociationField::new('bitrate', 'label.bitrate')->setRequired(true)->setHelp('Audio quality for this artist');
        yield BooleanField::new('alert', 'label.alert')
                          ->setHelp('If checked, you will receive an email alert when artist have new release. (need to configure email alerting)');
        yield DateTimeField::new('startedMonitoringAt', 'label.start_monitoring_at')->hideOnForm();
        yield FormField::addPanel('label.albums')->hideOnForm();
        yield AssociationField::new('albums', 'label.albums')->setTemplatePath('admin/crud/artist/fields/albums.html.twig')->hideOnForm()
                              ->hideOnIndex();
        yield AssociationField::new('albums', 'label.albums')->hideOnForm()->hideOnDetail()->setSortable(true);
        yield FormField::addPanel('label.tracks')->hideOnForm();
        yield AssociationField::new('tracks', 'label.tracks')->setTemplatePath('admin/crud/artist/fields/tracks.html.twig')->hideOnForm()
                              ->hideOnIndex();
        yield AssociationField::new('tracks', 'label.tracks')->hideOnForm()->hideOnDetail()->setSortable(true);
    }

    public static function getEntityFqcn(): string
    {
        return Artist::class;
    }
}
