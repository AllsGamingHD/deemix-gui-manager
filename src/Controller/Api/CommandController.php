<?php

namespace App\Controller\Api;

use App\Entity\Artist;
use App\Entity\Import;
use App\Entity\Parameters;
use App\Helper\NotifierHelper;
use App\Message\DeemixStarter;
use App\Message\DeemixStartSingleDownload;
use App\Provider\DeezerApiProvider;
use App\Repository\ParametersRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Routing\Annotation\Route;

class CommandController extends AbstractController
{
    #[Route('/api/start_deemix', name: 'api_start_deemix')]
    public function startDeemix(MessageBusInterface $bus): Response
    {
        $bus->dispatch(new DeemixStarter());
        return new Response(json_encode(['status' => 'success', 'message' => 'Received call'], JSON_THROW_ON_ERROR), Response::HTTP_OK, ['Content-Type' => 'application/json']);
    }

    #[Route('/api/start_deemix/single_download', name: 'api_start_deemix_single_download')]
    public function startDeemixSingleDownload(MessageBusInterface $bus, Request $request): Response
    {
        if (!$request->query->has('albumId')) return new Response('Need "albumId" parameter');

        $bus->dispatch((new DeemixStartSingleDownload())->setAlbumId($request->query->get('albumId')));
        return new Response(json_encode(['status' => 'success', 'message' => 'Received call'], JSON_THROW_ON_ERROR), Response::HTTP_OK, ['Content-Type' => 'application/json']);
    }//

    #[Route('/api/lang/{lang}')]
    public function setLang(string $lang, Request $request, EntityManagerInterface $entityManager): Response
    {
        $request->getSession()->set('_locale', $lang);
        $this->addFlash('success', 'Switched to language : ' . $lang);
        return new Response(json_encode(['status' => 'success', 'message' => 'Received call'], JSON_THROW_ON_ERROR), Response::HTTP_OK, ['Content-Type' => 'application/json']);
    }

    #[Route('/api/deep_import/{type}/{id}')]
    public function deepImport(string $type, int $id, EntityManagerInterface $entityManager, DeezerApiProvider $deezerApi, NotifierHelper $notifierHelper): Response
    {
        if ($type === 'artist')
        {
            $artist = $entityManager->getRepository(Artist::class)->findOneBy(['id' => $id]);
            $notifierHelper->setContent('Start deep import', "Deep import for artist {$artist->getName()}")->notify();
            foreach ($artist->getAlbums() as $album)
            {
                $dzAlbumInfo = $deezerApi->getAlbumById($album->getAlbumId());
                $notifierHelper->setContent('Album deep import', "Album deep import of [{$album->getName()}] form {$artist->getName()}")->notify();
                $album->setUpc($dzAlbumInfo['upc'] ?? null)
                      ->setGenres(array_column($dzAlbumInfo['genres']['data'] ?? ['name' => null], 'name'))
                      ->setLabel($dzAlbumInfo['label'] ?? null)
                      ->setNbTracks($dzAlbumInfo['nb_tracks'] ?? null)
                      ->setDuration($dzAlbumInfo['duration'] ?? null)
                      ->setRating($dzAlbumInfo['rating'] ?? null)
                      ->setIsAvailable($dzAlbumInfo['available'] ?? null)
                      ->setIsExplicit($dzAlbumInfo['explicit_lyrics'])
                      ->setIsExplicitContent($dzAlbumInfo['explicit_content_lyrics'] ?? null)
                      ->setIsExplicitCover($dzAlbumInfo['explicit_content_cover'] ?? null)
                      ->setContributors(array_column($dzAlbumInfo['contributors'] ?? ['name' => null], 'name') ?? []);
                foreach ($album->getTracks() as $track)
                {
                    $dzTrackInfo = $deezerApi->getTrackById($track->getTrackId());
                    $track->setBpm($dzTrackInfo['bpm'] ?? null)
                          ->setGain($dzTrackInfo['gain'] ?? null)
                          ->setContributors(array_column($dzTrackInfo['contributors'] ?? ['name' => null], 'name'))
                          ->setAvailableIn($dzTrackInfo['available_countries'] ?? null);
                }

            }
            $artist->setImport($entityManager->getRepository(Import::class)->findOneBy(['id' => 3]));
            $entityManager->flush();
            $notifierHelper->setContent('Finished deep import', "Deep import for artist {$artist->getName()} finished")->notify();
        }
        return new Response(json_encode(['status' => 'success', 'message' => 'Received call'], JSON_THROW_ON_ERROR), Response::HTTP_OK, ['Content-Type' => 'application/json']);
    }
}
