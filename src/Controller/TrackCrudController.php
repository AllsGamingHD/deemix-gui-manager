<?php

namespace App\Controller;

use App\Entity\Track;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\ArrayField;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\BooleanField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IntegerField;
use EasyCorp\Bundle\EasyAdminBundle\Field\NumberField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;

class TrackCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Track::class;
    }

    public function configureActions(Actions $actions): Actions
    {
        return $actions
            ->add(Crud::PAGE_INDEX, Action::DETAIL)
            ->disable(Crud::PAGE_NEW)
            ->remove(Crud::PAGE_INDEX, Action::NEW)->remove(Crud::PAGE_INDEX, Action::DELETE)
            ->remove(Crud::PAGE_DETAIL, Action::DELETE)
            ->disable(Crud::PAGE_NEW)
            ->disable(Crud::PAGE_EDIT);
    }

    public function configureCrud(Crud $crud): Crud
    {
        return $crud->setPageTitle('index', 'label.list_of_tracks')
                    ->setPageTitle('edit', fn(Track $album) => 'Edit track : ' . $album->getTitle())
                    ->setPageTitle('detail', fn(Track $album) => 'Detail of : ' . $album->getTitle())
                    ->setDateTimeFormat($this->get('session')->get('_locale') === 'fr' ? 'dd/MM/Y @ H:mm:ss' : 'Y-MM-dd @ H:mm:ss')
                    ->setDateFormat($this->get('session')->get('_locale') === 'fr' ? 'dd/MM/Y' : 'Y-MM-dd');
    }

    public function configureFields(string $pageName): iterable
    {
        yield IntegerField::new('track_id', 'label.track_id')->setFormTypeOptions(['attr' => ['disabled' => true]]);
        yield TextField::new('title', 'label.title')->setFormTypeOptions(['attr' => ['disabled' => true]]);
        yield TextField::new('title_short', 'label.title_short')->hideOnIndex()->setFormTypeOptions(['attr' => ['disabled' => true]]);
        yield TextField::new('title_version', 'label.title_version')->hideOnIndex()->setFormTypeOptions(['attr' => ['disabled' => true]]);
        yield TextField::new('url', 'label.url')->hideOnIndex()->setFormTypeOptions(['attr' => ['disabled' => true]]);
        yield IntegerField::new('durationFormatted', 'label.duration')->hideOnForm();
        yield IntegerField::new('duration', 'label.duration_in_second')->onlyOnForms()->setFormTypeOptions(['attr' => ['disabled' => true]]);
        yield IntegerField::new('rankPourcentage', 'label.rating')->hideOnForm();
        yield IntegerField::new('rank', 'label.rating')->onlyOnForms()->setFormTypeOptions(['attr' => ['disabled' => true]]);
        yield BooleanField::new('is_explicit_lyrics', 'label.track_explicit_content')->renderAsSwitch(false)
                          ->setFormTypeOptions(['attr' => ['disabled' => true]]);
        yield TextField::new('isrc', 'ISRC')->hideOnForm()->hideOnIndex();
        yield IntegerField::new('track_position', 'label.track_position')->hideOnForm()->hideOnIndex();
        yield IntegerField::new('disk_number', 'label.disk_number')->hideOnForm()->hideOnIndex();
        yield DateField::new('released_at', 'label.released_at')->hideOnForm()->hideOnIndex();
        yield IntegerField::new('bpm', 'label.bpm')->hideOnForm()->hideOnIndex();
        yield NumberField::new('gain', 'label.gain')->hideOnForm()->hideOnIndex();
        yield ArrayField::new('contributors', 'label.contributors')->hideOnForm()->hideOnIndex();
        yield ArrayField::new('available_in', 'label.available_in_countries')->hideOnForm()->hideOnIndex()
                        ->setTemplatePath('/admin/fields/countryIcon.html.twig');
        yield TextField::new('preview_url', 'label.preview_url')->hideOnForm()->hideOnIndex();
        yield AssociationField::new('album', 'label.album')->setTemplatePath('admin/crud/artist/fields/albums.html.twig')->hideOnForm()
                              ->hideOnIndex();
    }
}
