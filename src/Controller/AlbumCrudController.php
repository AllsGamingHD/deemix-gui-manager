<?php

namespace App\Controller;

use App\Entity\Album;
use Doctrine\ORM\EntityManagerInterface;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Config\Filters;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\ArrayField;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\BooleanField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateTimeField;
use EasyCorp\Bundle\EasyAdminBundle\Field\FormField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ImageField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IntegerField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Filter\ChoiceFilter;
use Symfony\Contracts\Translation\TranslatorInterface;

class AlbumCrudController extends AbstractCrudController
{
    private ?EntityManagerInterface $entityManager;
    private ?TranslatorInterface $translator;

    public function __construct(EntityManagerInterface $entityManager, TranslatorInterface $translator)
    {
        $this->entityManager = $entityManager;
        $this->translator    = $translator;
    }

    public static function getEntityFqcn(): string
    {
        return Album::class;
    }

    public function configureFilters(Filters $filters): Filters
    {
        return $filters->add(
            ChoiceFilter::new('isDownloaded')->setChoices([
                                                              'Failed to download' => 3,
                                                              'Downloaded'         => 2,
                                                              'In download'        => 1,
                                                              'Not downloaded'     => 0,
                                                          ])->setLabel('Download state')
        );
    }

    public function configureActions(Actions $actions): Actions
    {
        return $actions
            ->add(Crud::PAGE_INDEX, Action::DETAIL)
            ->disable(Crud::PAGE_NEW)
            ->remove(Crud::PAGE_INDEX, Action::NEW)
            ->remove(Crud::PAGE_DETAIL, Action::DELETE)
            ->disable(Crud::PAGE_NEW)
            ->disable(Crud::PAGE_EDIT);
    }

    public function configureCrud(Crud $crud): Crud
    {
        $albumCount = $this->entityManager->getRepository(Album::class)->getTotalCountAndTotalDownloaded();
        return $crud->setPageTitle('index', $this->translator->trans('label.list_of_albums') . " - ({$albumCount['totalDownloaded']}/{$albumCount['totalWithNotFailed']}) - {$albumCount['totalFailedDownload']} Failed - {$albumCount['totalInDownload']} in download")
                    ->setPageTitle('edit', fn(Album $album) => 'Edit album : ' . $album->getName())
                    ->setPageTitle('detail', fn(Album $album) => 'Detail of : ' . $album->getName())
                    ->setDateTimeFormat($this->get('session')->get('_locale') === 'fr' ? 'dd/MM/Y @ H:mm:ss' : 'Y-MM-dd @ H:mm:ss')
                    ->setDateFormat($this->get('session')->get('_locale') === 'fr' ? 'dd/MM/Y' : 'Y-MM-dd');
    }

    public function configureFields(string $pageName): iterable
    {

        yield IntegerField::new('isDownloaded', 'label.status.downloaded')->setTemplatePath('admin/fields/isDownloaded.html.twig')->hideOnForm();
        yield IntegerField::new('album_id', 'label.album_id')->setFormTypeOptions(['attr' => ['disabled' => true]])->setColumns(6);
        yield TextField::new('name', 'label.album_name')->setFormTypeOptions(['attr' => ['disabled' => true]])->setColumns(6);
        yield TextField::new('upc')->onlyOnDetail()->setFormTypeOptions(['attr' => ['disabled' => true]])->setColumns(12);
        yield TextField::new('url', 'label.url')->onlyOnDetail()->setFormTypeOptions(['attr' => ['disabled' => true]])->setColumns(12);
        yield ImageField::new('cover_url', 'label.album_cover')->onlyOnDetail();
        yield ArrayField::new('genres', 'label.genres')->setFormTypeOptions(['attr' => ['disabled' => true]])->setColumns(12);
        yield TextField::new('label', 'label.album_label')->onlyOnDetail()->setFormTypeOptions(['attr' => ['disabled' => true]])->setColumns(12);
        yield IntegerField::new('nb_tracks', 'label.album.tracks_number')->setFormTypeOptions(['attr' => ['disabled' => true]])->setColumns(3);
        yield IntegerField::new('durationFormatted', 'label.album.total_duration')->onlyOnIndex()->onlyOnDetail();
        yield IntegerField::new('duration', 'label.album.total_duration_in_seconds')->onlyOnForms()
                          ->setFormTypeOptions(['attr' => ['disabled' => true]])->setColumns(3);
        yield IntegerField::new('nb_fans', 'label.number_of_fans')->setFormTypeOptions(['attr' => ['disabled' => true]])->setColumns(3);
        yield IntegerField::new('rating')->hideOnForm()->hideOnDetail()->hideOnIndex()->setFormTypeOptions(['attr' => ['disabled' => true]])
                          ->setColumns(3);
        yield BooleanField::new('is_explicit', 'label.album_is_explicit')->renderAsSwitch(false)->setFormTypeOptions(['attr' => ['disabled' => true]])
                          ->setColumns(4);
        yield BooleanField::new('is_explicit_content', 'label.album_is_explicit_content')->renderAsSwitch(false)
                          ->setFormTypeOptions(['attr' => ['disabled' => true]])->hideOnIndex()->setColumns(4);
        yield BooleanField::new('is_explicit_cover', 'label.album_is_explicit_cover')->renderAsSwitch(false)
                          ->setFormTypeOptions(['attr' => ['disabled' => true]])->hideOnIndex()->setColumns(4);
        yield ArrayField::new('contributors', 'label.contributors')->setFormTypeOptions(['attr' => ['disabled' => true]])->hideOnForm()
                        ->setColumns(12);
        yield DateField::new('released_at', 'label.released_at')->setFormTypeOptions(['attr' => ['disabled' => true]])->setColumns(6)
                       ->setSortable(true);
        yield DateTimeField::new('started_monitoring_at', 'label.start_monitoring_at')->setFormTypeOptions(['attr' => ['disabled' => true]])
                           ->setColumns(6)
                           ->setSortable(true);
        yield FormField::addPanel('Tracks')->hideOnForm();
        yield AssociationField::new('tracks', 'label.tracks')->setTemplatePath('admin/crud/artist/fields/tracks.html.twig')->hideOnForm()
                              ->hideOnIndex();
        yield AssociationField::new('tracks', 'label.tracks')->hideOnForm()->hideOnDetail();

    }
}
