<?php

namespace App\Controller;

use App\Message\Monitor;
use App\Provider\DeezerApiProvider;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Routing\Annotation\Route;

class DeezerSearcherController extends AbstractController
{
    #[Route('/deezer/searcher', name: 'deezer_searcher')]
    public function index(): Response
    {
        return $this->render('deezer_searcher/index.html.twig');
    }

    #[Route('/deezer/api/search', name: 'deezer_search_api')]
    public function search(Request $request, DeezerApiProvider $deezerApi)
    {
        $queryParam = $request->get('query');
        $typeParam  = $request->get('type');

        $query = '';
        if ($typeParam !== null && $typeParam !== 'all')
        {
            $query .= '/' . $typeParam . '';
        }
        $query .= '?' . http_build_query(['q' => $queryParam]);
        return new Response(json_encode($deezerApi->search($query), JSON_THROW_ON_ERROR), Response::HTTP_OK, ['Content-Type' => 'application/json']);
    }

    #[Route('/api/{type}/{id}', name: 'api_monitor_artist')]
    public function monitor(string $type, int $id, MessageBusInterface $bus, Request $request): Response
    {
        $importOpt = $request->query->get('importOpt');
        $monitor = new Monitor();
        $monitor->setType($type);
        $monitor->setId($id);
        $monitor->setOpt($importOpt);
        $bus->dispatch($monitor);
        return new Response('ok');
    }
}
