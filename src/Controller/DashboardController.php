<?php

namespace App\Controller;

use App\Entity\Album;
use App\Entity\Artist;
use App\Entity\Parameters;
use App\Entity\Track;
use App\Repository\ParametersRepository;
use Doctrine\ORM\EntityManagerInterface;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Config\Dashboard;
use EasyCorp\Bundle\EasyAdminBundle\Config\MenuItem;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractDashboardController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

class DashboardController extends AbstractDashboardController
{
    public function __construct(EntityManagerInterface $entityManager, RequestStack $requestStack, TranslatorInterface $translator)
    {
        $this->parameters = $entityManager->getRepository(Parameters::class)->findLast();
        if (!$this->parameters)
        {
            $this->parameters = new Parameters();
            $entityManager->persist($this->parameters);
            $entityManager->flush();
        }
        $this->currentParametersId = $this->parameters->getId();
        $requestStack->getSession()->set('parameters', $this->parameters);
        ini_set('date.timezone', $this->parameters->getTimeZone());
        ini_set('memory_limit', '4G');
    }


    /**
     * @Route("/", name="home")
     */
    public function index(): Response
    {
        return parent::index();
    }

    public function configureDashboard(): Dashboard
    {
        return Dashboard::new()
                        ->setTitle('Deemix Gui Manager')->disableUrlSignatures();
    }

    public function configureCrud(): Crud
    {
        return Crud::new()->overrideTemplates(
            [
                'crud/index'  => 'bundles/EasyAdminBundle/crud/index.html.twig',
                'crud/detail' => 'bundles/EasyAdminBundle/crud/detail.html.twig',
                'crud/edit'   => 'bundles/EasyAdminBundle/crud/edit.html.twig',
                'crud/new'    => 'bundles/EasyAdminBundle/crud/new.html.twig',
            ]
        );
    }


    public function configureMenuItems(): iterable
    {
        yield MenuItem::linktoDashboard('label.dashboard', 'fa fa-home');

        yield MenuItem::section('label.monitoring', 'fa fa-database');
        yield MenuItem::linkToCrud('label.artists', 'fa fa-user-tie', Artist::class);
        yield MenuItem::linkToCrud('label.albums', 'fa fa-compact-disc', Album::class);
        if ($this->parameters->getDeepDataGrap()->getId() !== 1)
        {
            yield MenuItem::linkToCrud('label.tracks', 'fab fa-itunes-note', Track::class);
        }

        yield MenuItem::section('label.deezer_search_tool', 'fab fa-deezer');
        yield MenuItem::linkToRoute('label.deezer_search_tool.search', 'fa fa-search', 'deezer_searcher');

        yield MenuItem::section('label.tools', 'fa fa-toolbox');
        yield MenuItem::linkToCrud('label.parameters', 'fa fa-user-cog', Parameters::class)->setAction(Crud::PAGE_EDIT)
                      ->setEntityId($this->parameters->getId());
        // yield MenuItem::linkToCrud('The Label', 'fas fa-list', EntityClass::class);
        yield MenuItem::section('label.stats', 'fa fa-chart-pie');
        yield MenuItem::linkToRoute('label.see_monitor_stats', 'fa fa-chart-bar', 'stats_monitor');
        yield MenuItem::linkToRoute('label.see_download_stats', 'fa fa-chart-bar', 'stats_download');
        yield MenuItem::linkToRoute('label.see_albums_stats', 'fa fa-chart-bar', 'stats_albums');
    }

    #[Route('/stats/monitor', name: 'stats_monitor')]
    public function statsMonitor(EntityManagerInterface $entityManager): Response
    {
        return $this->render('stats/monitor.html.twig');
    }

    #[Route('/stats/download', name: 'stats_download')]
    public function statsDownload(EntityManagerInterface $entityManager): Response
    {
        return $this->render('stats/download.html.twig');
    }


    #[Route('/stats/albums/{yearStart}/{yearEnd}')]
    public function statsAlbumsRange(int $yearStart, int $yearEnd, EntityManagerInterface $entityManager): Response
    {
        $albums = $entityManager->getRepository(Album::class)->getAlbumReleasedAtMonitor($yearStart, $yearEnd);
        return new Response(json_encode($albums), Response::HTTP_OK, ['Content-Type' => 'application/json']);
    }

    #[Route('/stats/albums', name: 'stats_albums')]
    public function statsAlbums(EntityManagerInterface $entityManager): Response
    {
        $albums = $entityManager->getRepository(Album::class)->getAlbumReleasedAtMonitor();
        return $this->render('stats/albums.html.twig', ['albums' => $albums]);
    }


    /**
     * @throws \JsonException
     */
    #[Route('/api/stats/{type}')]
    public function getStat(string $type, EntityManagerInterface $entityManager, Request $request): Response
    {
        $startDate = new \DateTime($request->query->get('startDate'));
        $endDate   = new \DateTime($request->query->get('endDate'));
        $diff      = $startDate->diff($endDate);

        $secondSeparation = match (true)
        {
            $diff->days === 0 && $diff->h <= 1 => 150, // under ho
            $diff->days === 0 && $diff->h <= 6 => 900, // TODAY
            $diff->days === 0 && $diff->h <= 24 => 3600, // IS A DAY ONLY (24h)
            $diff->days === 0 && $diff->h === 0 && $diff->i < 1 => 1,
            $diff->days === 0 && $diff->h === 0 && $diff->i === 1 => 5, // 1 Min (SEPARATE by 5sec)
            $diff->days === 0 && $diff->h === 0 && $diff->i <= 5 => 30, // 5 Min (SEPARATE by 30sec)
            $diff->days === 0 && $diff->h === 0 && $diff->i <= 30 => 60, // 30 Min (SEPARATE by 60sec)
            // 6 Hours (SEPARATE by 10min)
            $diff->days === 1 => 1800, // DAY (SEPARATE by 30min)
            $diff->days < 31 => 86400, // MONTH (SEPARATE by day)
            $diff->days <= 1461 && $diff->y <= 4 => 2592000, // YEAR (SEPARATE by 30 day)
            default => 31540000 // OVER YEAR (SEPARATE by YEAR)
        };
        $data             = [];
        if ($secondSeparation === 31540000) $duration = 'P1Y';
        else if ($secondSeparation === 2592000) $duration = 'P1M';
        else $duration = 'PT' . $secondSeparation . 'S';

        $date = new \DateTime($request->query->get('startDate'));
        while ($date <= $endDate)
        {
            $showValue = $this->getShowValue($secondSeparation, $date->format('Y-m-d H:i:s'));

            $data[$date->format('Y-m-d H:i:s')] = [
                "datetime"  => $date->format('Y-m-d H:i:s'),
                "date"      => $date->format('Y-m-d'),
                "time"      => $date->format('H:i'),
                "showValue" => $showValue,
                "total"     => "0",
            ];
            $date->add(new \DateInterval($duration));
        }

        if ($type === 'monitor')
        {
            $artistsMonitoringStats = $entityManager->getRepository(Artist::class)
                                                    ->getLastXMinutesMonitoring($secondSeparation, $startDate, $endDate);
            $albumsMonitoringStats  = $entityManager->getRepository(Album::class)
                                                    ->getLastXMinutesMonitoring($secondSeparation, $startDate, $endDate);
            $trackMonitoringStats   = $entityManager->getRepository(Track::class)
                                                    ->getLastXMinutesMonitoring($secondSeparation, $startDate, $endDate);
        }
        else if ($type === 'download')
        {
            $artistsMonitoringStats = $entityManager->getRepository(Artist::class)
                                                    ->getLastXMinutesDownloaded($secondSeparation, $startDate, $endDate);
            $albumsMonitoringStats  = $entityManager->getRepository(Album::class)
                                                    ->getLastXMinutesDownloaded($secondSeparation, $startDate, $endDate);
        }
        $response = [
            'artist' => $artistsMonitoringStats ?? [],
            'album'  => $albumsMonitoringStats ?? [],
            'track'  => $trackMonitoringStats ?? [],
        ];

        $finalResponse = [];
        foreach ($response as $type => $stats)
        {
            if (!isset($finalResponse[$type])) $finalResponse[$type] = $data;
            foreach ($stats as $rangeDate)
            {
                $showValue                                    = $this->getShowValue($secondSeparation, $rangeDate['datetime']);
                $rangeDate['showValue']                       = $showValue;
                $finalResponse[$type][$rangeDate['datetime']] = $rangeDate;
            }
        }
        return new Response(json_encode($finalResponse ?? $response, JSON_THROW_ON_ERROR), Response::HTTP_OK, ['Content-Type' => 'application/json']);
    }

    private function getShowValue(int $number, $date)
    {
        $date = new \DateTime($date);
        if ($number === 31540000) $showValue = $date->format('Y');
        else if ($number === 2592000) $showValue = $date->format('F Y');
        else if ($number === 86400) $showValue = $date->format('Y-m-d');
        else if ($number === 600 || $number === 3600) $showValue = $date->format('H:i');
        else $showValue = $date->format('Y-m-d H:i:s');
        return $showValue;
    }
}
