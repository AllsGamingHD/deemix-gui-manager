<?php

namespace App\Controller;

use App\Entity\Parameters;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Context\AdminContext;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\BooleanField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ChoiceField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateField;
use EasyCorp\Bundle\EasyAdminBundle\Field\FormField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IntegerField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Router\AdminUrlGenerator;

class ParametersCrudController extends AbstractCrudController
{
    private ?AdminUrlGenerator $adminUrlGenerator;

    public function __construct(AdminUrlGenerator $adminUrlGenerator)
    {
        $this->adminUrlGenerator = $adminUrlGenerator;
    }

    public function configureCrud(Crud $crud): Crud
    {
        return $crud->setDateTimeFormat($this->get('session')->get('_locale') === 'fr' ? 'dd/MM/Y @ H:mm:ss' : 'Y-MM-dd @ H:mm:ss')
                    ->setDateFormat($this->get('session')->get('_locale') === 'fr' ? 'dd/MM/Y' : 'Y-MM-dd');
    }

    public static function getEntityFqcn(): string
    {
        return Parameters::class;
    }

    public function edit(AdminContext $context)
    {
        if ($context->getRequest()->getMethod() === 'POST')
        {
            parent::edit($context);
            $this->addFlash('success', 'Parameters successfully saved !');
            return $this->redirect($this->adminUrlGenerator->setController(__CLASS__)->setAction(Action::EDIT)->setEntityId($context->getEntity()
                                                                                                                                    ->getInstance()
                                                                                                                                    ->getId()));
        }
        return parent::edit($context);
    }

    public function configureActions(Actions $actions): Actions
    {
        return $actions->disable(Crud::PAGE_DETAIL)->disable(Crud::PAGE_NEW)->disable(Crud::PAGE_INDEX)
                       ->remove(Crud::PAGE_EDIT, Action::SAVE_AND_CONTINUE);
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            ChoiceField::new('timeZone', 'label.timezone')->setChoices(array_combine(timezone_identifiers_list(), timezone_identifiers_list()))
                       ->hideOnForm(),
            FormField::addPanel('Deemon configuration'),
            TextField::new('downloadPath', 'label.download_path')->setHelp('Where artists, playlists musics while be downloaded by default')
                     ->setColumns('col-md-12'),
            TextField::new('deemixCommand', 'label.deemix_command'),

            FormField::addPanel('label.import_config'),
            AssociationField::new('deepDataGrap', 'label.deep_data_grap'),
            //->setHelp('If activated when importing an artist, you will get more information about artists, albums and tracks. (More information is about to take more time to import estimated to take 60% extra time)'),

            FormField::addPanel('label.deezer_config'),
            BooleanField::new('canDownload', 'If configuration is not configured you cannot download')
                        ->setHelp('Add your ARL and deemix path to download')->setColumns('col-lg-12'),
            TextField::new('arl', 'label.deezer_arl')->setHelp('Get you ARL from deezer in cookies')->setColumns('col-lg-6'),
            TextField::new('deemixPath', 'label.deemix_access_path')->setHelp('Need to have your deemix access absolute path')
                     ->setColumns('col-lg-6'),
            AssociationField::new('bitrate', 'label.default_bitrate')->setRequired(true)->setHelp('Audio quality for this artist')
                            ->setColumns('col-lg-6'),
            AssociationField::new('recordType', 'label.default_record_type')->setRequired(true)->setHelp('Type of record for this artist')
                            ->setColumns('col-lg-6'),

            FormField::addPanel('label.refresh_config'),
            BooleanField::new('checkReleaseByDate', 'label.check_with_release_date')
                        ->setHelp('If is checked deemon will check in limit of below date')
                        ->setColumns('col-md-12'),
            DateField::new('checkReleaseByDateMaxDays', 'label.max_release_date')
                     ->setHelp('The date where deemon while check if releases is not older than this date')->setColumns('col-md-12'),

            FormField::addPanel('SMTP Configuration'),
            BooleanField::new('alerting', 'label.alerting')->setHelp('Check if you want to get alerts by default')->setColumns('col-md-12'),
            TextField::new('smtpServer', 'SMTP Server')->setColumns('col-md-6'),
            IntegerField::new('smtpPort', 'SMTP Port')->setColumns('col-md-6'),
            TextField::new('smtpUser', 'SMTP User')->setColumns(6),
            TextField::new('smtpPassword', 'SMTP Password')->setColumns(6),
            TextField::new('smtpEmailFrom', 'SMTP Email From')->setColumns(6),
            TextField::new('smtpEmailTo', 'SMTP Email To')->setColumns(6),
        ];
    }
}
