<?php

namespace App\Entity;

use App\Repository\TrackRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=TrackRepository::class)
 */
class Track
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $trackId;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $title;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $titleShort;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $titleVersion;

    /**
     * @ORM\Column(type="text")
     */
    private $url;

    /**
     * @ORM\Column(type="integer")
     */
    private $duration;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $rank;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isExplicitLyrics;

    /**
     * @ORM\ManyToOne(targetEntity=Artist::class, inversedBy="tracks")
     * @ORM\JoinColumn(nullable=false)
     */
    private $artist;

    /**
     * @ORM\ManyToMany(targetEntity=Album::class, inversedBy="tracks")
     */
    private $album;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $isrc;

    /**
     * @ORM\Column(type="integer")
     */
    private $trackPosition;

    /**
     * @ORM\Column(type="integer")
     */
    private $diskNumber;

    /**
     * @ORM\Column(type="date_immutable", nullable=true)
     */
    private $releasedAt;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $bpm;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $gain;

    /**
     * @ORM\Column(type="simple_array", nullable=true)
     */
    private $contributors = null;

    /**
     * @ORM\Column(type="simple_array", nullable=true)
     */
    private $availableIn = null;

    /**
     * @ORM\Column(type="text")
     */
    private $previewUrl;

    /**
     * @ORM\Column(type="datetime_immutable")
     */
    private $addedAt;

    public function __construct()
    {
        $this->album = new ArrayCollection();
        $this->setAddedAt(new \DateTimeImmutable());
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDurationFormatted()
    {
        return $this->duration !== null ? gmdate("i:s", $this->duration) : null;
    }

    public function getRankPourcentage()
    {
        return round($this->rank / 10000, 2) . '%';
    }

    public function getArtist(): ?Artist
    {
        return $this->artist;
    }

    public function setArtist(?Artist $artist): self
    {
        $this->artist = $artist;

        return $this;
    }

    /**
     * @return Collection|Album[]
     */
    public function getAlbum(): Collection
    {
        return $this->album;
    }

    public function addAlbum(Album $album): self
    {
        if (!$this->album->contains($album))
        {
            $this->album[] = $album;
        }

        return $this;
    }

    public function removeAlbum(Album $album): self
    {
        $this->album->removeElement($album);

        return $this;
    }

    public function __toString(): string
    {
        return $this->getTitle();
    }

    /**
     * @return mixed
     */
    public function getTrackId()
    {
        return $this->trackId;
    }

    /**
     * @param mixed $trackId
     *
     * @return Track
     */
    public function setTrackId($trackId)
    {
        $this->trackId = $trackId;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param mixed $title
     *
     * @return Track
     */
    public function setTitle($title)
    {
        $this->title = $title;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getTitleShort()
    {
        return $this->titleShort;
    }

    /**
     * @param mixed $titleShort
     *
     * @return Track
     */
    public function setTitleShort($titleShort)
    {
        $this->titleShort = $titleShort;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getTitleVersion()
    {
        return $this->titleVersion;
    }

    /**
     * @param mixed $titleVersion
     *
     * @return Track
     */
    public function setTitleVersion($titleVersion)
    {
        $this->titleVersion = $titleVersion;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @param mixed $url
     *
     * @return Track
     */
    public function setUrl($url)
    {
        $this->url = $url;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDuration()
    {
        return $this->duration;
    }

    /**
     * @param mixed $duration
     *
     * @return Track
     */
    public function setDuration($duration)
    {
        $this->duration = $duration;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getRank()
    {
        return $this->rank;
    }

    /**
     * @param mixed $rank
     *
     * @return Track
     */
    public function setRank($rank)
    {
        $this->rank = $rank;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getIsExplicitLyrics()
    {
        return $this->isExplicitLyrics;
    }

    /**
     * @param mixed $isExplicitLyrics
     *
     * @return Track
     */
    public function setIsExplicitLyrics($isExplicitLyrics)
    {
        $this->isExplicitLyrics = $isExplicitLyrics;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getIsrc()
    {
        return $this->isrc;
    }

    /**
     * @param mixed $isrc
     *
     * @return Track
     */
    public function setIsrc($isrc)
    {
        $this->isrc = $isrc;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getTrackPosition()
    {
        return $this->trackPosition;
    }

    /**
     * @param mixed $trackPosition
     *
     * @return Track
     */
    public function setTrackPosition($trackPosition)
    {
        $this->trackPosition = $trackPosition;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDiskNumber()
    {
        return $this->diskNumber;
    }

    /**
     * @param mixed $diskNumber
     *
     * @return Track
     */
    public function setDiskNumber($diskNumber)
    {
        $this->diskNumber = $diskNumber;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getReleasedAt()
    {
        return $this->releasedAt;
    }

    /**
     * @param mixed $releasedAt
     *
     * @return Track
     */
    public function setReleasedAt($releasedAt)
    {
        $this->releasedAt = $releasedAt;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getBpm()
    {
        return $this->bpm;
    }

    /**
     * @param mixed $bpm
     *
     * @return Track
     */
    public function setBpm($bpm)
    {
        $this->bpm = $bpm;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getGain()
    {
        return $this->gain;
    }

    /**
     * @param mixed $gain
     *
     * @return Track
     */
    public function setGain($gain)
    {
        $this->gain = $gain;
        return $this;
    }

    /**
     * @return null
     */
    public function getContributors()
    {
        return $this->contributors;
    }

    /**
     * @param null $contributors
     *
     * @return Track
     */
    public function setContributors($contributors)
    {
        $this->contributors = $contributors;
        return $this;
    }

    /**
     * @return null
     */
    public function getAvailableIn()
    {
        return $this->availableIn;
    }

    /**
     * @param null $availableIn
     *
     * @return Track
     */
    public function setAvailableIn($availableIn)
    {
        $this->availableIn = $availableIn;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPreviewUrl()
    {
        return $this->previewUrl;
    }

    /**
     * @param mixed $previewUrl
     *
     * @return Track
     */
    public function setPreviewUrl($previewUrl)
    {
        $this->previewUrl = $previewUrl;
        return $this;
    }

    public function getAddedAt(): ?\DateTimeImmutable
    {
        return $this->addedAt;
    }

    public function setAddedAt(\DateTimeImmutable $addedAt): self
    {
        $this->addedAt = $addedAt;

        return $this;
    }
}
