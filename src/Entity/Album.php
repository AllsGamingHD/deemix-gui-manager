<?php

namespace App\Entity;

use App\Repository\AlbumRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=AlbumRepository::class)
 */
class Album
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $albumId;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $upc;

    /**
     * @ORM\Column(type="text")
     */
    private $url;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $coverUrl;

    /**
     * @ORM\Column(type="simple_array", nullable=true)
     */
    private $genres = null;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $label;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $nbTracks;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $duration;

    /**
     * @ORM\Column(type="integer")
     */
    private $nbFans;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $rating;

    /**
     * @ORM\Column(type="date_immutable")
     */
    private $releasedAt;

    /**
     * @ORM\ManyToOne(targetEntity=RecordType::class, inversedBy="albums")
     * @ORM\JoinColumn(nullable=true)
     */
    private $recordType;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $isAvailable;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isExplicit;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $isExplicitContent;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $isExplicitCover;

    /**
     * @ORM\Column(type="simple_array", nullable=true)
     */
    private $contributors = null;

    /**
     * @ORM\Column(type="datetime_immutable")
     */
    private $startedMonitoringAt;

    /**
     * @ORM\ManyToMany(targetEntity=Track::class, mappedBy="album", cascade={"remove"})
     */
    private $tracks;

    /**
     * @ORM\ManyToOne(targetEntity=Artist::class, inversedBy="albums")
     * @ORM\JoinColumn(nullable=false)
     */
    private $artist;

    /**
     * @ORM\Column(type="integer")
     */
    private $isDownloaded = 0;

    /**
     * @ORM\ManyToOne(targetEntity=AudioBitrate::class, inversedBy="albums")
     * @ORM\JoinColumn(nullable=false)
     */
    private $bitrate;

    /**
     * @ORM\Column(type="datetime_immutable")
     */
    private $addedAt;

    /**
     * @ORM\Column(type="datetime_immutable", nullable=true)
     */
    private $downloadedOn;

    public function __construct()
    {
        $this->startedMonitoringAt = new \DateTimeImmutable();
        $this->tracks              = new ArrayCollection();
        $this->setAddedAt(new \DateTimeImmutable());
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDurationFormatted()
    {
        return $this->duration !== null ? gmdate("i:s", $this->duration) : null;
    }

    /**
     * @return Collection|Track[]
     */
    public function getTracks(): Collection
    {
        return $this->tracks;
    }

    public function addTrack(Track $track): self
    {
        if (!$this->tracks->contains($track))
        {
            $this->tracks[] = $track;
            $track->addAlbum($this);
        }

        return $this;
    }

    public function removeTrack(Track $track): self
    {
        if ($this->tracks->removeElement($track))
        {
            $track->removeAlbum($this);
        }

        return $this;
    }

    public function getArtist(): ?Artist
    {
        return $this->artist;
    }

    public function setArtist(?Artist $artist): self
    {
        $this->artist = $artist;

        return $this;
    }

    public function __toString(): string
    {
        return $this->getName();
    }

    /**
     * @return mixed
     */
    public function getAlbumId()
    {
        return $this->albumId;
    }

    /**
     * @param mixed $albumId
     *
     * @return Album
     */
    public function setAlbumId($albumId)
    {
        $this->albumId = $albumId;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     *
     * @return Album
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getUpc()
    {
        return $this->upc;
    }

    /**
     * @param mixed $upc
     *
     * @return Album
     */
    public function setUpc($upc)
    {
        $this->upc = $upc;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @param mixed $url
     *
     * @return Album
     */
    public function setUrl($url)
    {
        $this->url = $url;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCoverUrl()
    {
        return $this->coverUrl;
    }

    /**
     * @param mixed $coverUrl
     *
     * @return Album
     */
    public function setCoverUrl($coverUrl)
    {
        $this->coverUrl = $coverUrl;
        return $this;
    }

    /**
     * @return null
     */
    public function getGenres()
    {
        return $this->genres;
    }

    /**
     * @param null $genres
     *
     * @return Album
     */
    public function setGenres($genres)
    {
        $this->genres = $genres;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getLabel()
    {
        return $this->label;
    }

    /**
     * @param mixed $label
     *
     * @return Album
     */
    public function setLabel($label)
    {
        $this->label = $label;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getNbTracks()
    {
        return $this->nbTracks;
    }

    /**
     * @param mixed $nbTracks
     *
     * @return Album
     */
    public function setNbTracks($nbTracks)
    {
        $this->nbTracks = $nbTracks;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDuration()
    {
        return $this->duration;
    }

    /**
     * @param mixed $duration
     *
     * @return Album
     */
    public function setDuration($duration)
    {
        $this->duration = $duration;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getNbFans()
    {
        return $this->nbFans;
    }

    /**
     * @param mixed $nbFans
     *
     * @return Album
     */
    public function setNbFans($nbFans)
    {
        $this->nbFans = $nbFans;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getRating()
    {
        return $this->rating;
    }

    /**
     * @param mixed $rating
     *
     * @return Album
     */
    public function setRating($rating)
    {
        $this->rating = $rating;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getReleasedAt()
    {
        return $this->releasedAt;
    }

    /**
     * @param mixed $releasedAt
     *
     * @return Album
     */
    public function setReleasedAt($releasedAt)
    {
        $this->releasedAt = $releasedAt;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getRecordType()
    {
        return $this->recordType;
    }

    /**
     * @param mixed $recordType
     *
     * @return Album
     */
    public function setRecordType($recordType)
    {
        $this->recordType = $recordType;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getIsAvailable()
    {
        return $this->isAvailable;
    }

    /**
     * @param mixed $isAvailable
     *
     * @return Album
     */
    public function setIsAvailable($isAvailable)
    {
        $this->isAvailable = $isAvailable;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getIsExplicit()
    {
        return $this->isExplicit;
    }

    /**
     * @param mixed $isExplicit
     *
     * @return Album
     */
    public function setIsExplicit($isExplicit)
    {
        $this->isExplicit = $isExplicit;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getIsExplicitContent()
    {
        return $this->isExplicitContent;
    }

    /**
     * @param mixed $isExplicitContent
     *
     * @return Album
     */
    public function setIsExplicitContent($isExplicitContent)
    {
        $this->isExplicitContent = $isExplicitContent;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getIsExplicitCover()
    {
        return $this->isExplicitCover;
    }

    /**
     * @param mixed $isExplicitCover
     *
     * @return Album
     */
    public function setIsExplicitCover($isExplicitCover)
    {
        $this->isExplicitCover = $isExplicitCover;
        return $this;
    }

    /**
     * @return null
     */
    public function getContributors()
    {
        return $this->contributors;
    }

    /**
     * @param null $contributors
     *
     * @return Album
     */
    public function setContributors($contributors)
    {
        $this->contributors = $contributors;
        return $this;
    }

    /**
     * @return \DateTimeImmutable
     */
    public function getStartedMonitoringAt(): \DateTimeImmutable
    {
        return $this->startedMonitoringAt;
    }

    /**
     * @param \DateTimeImmutable $startedMonitoringAt
     *
     * @return Album
     */
    public function setStartedMonitoringAt(\DateTimeImmutable $startedMonitoringAt): Album
    {
        $this->startedMonitoringAt = $startedMonitoringAt;
        return $this;
    }

    /**
     * @return int
     */
    public function getIsDownloaded(): int
    {
        return $this->isDownloaded;
    }

    /**
     * @param int $isDownloaded
     *
     * @return Album
     */
    public function setIsDownloaded(int $isDownloaded): Album
    {
        $this->isDownloaded = $isDownloaded;
        return $this;
    }

    public function getBitrate(): ?AudioBitrate
    {
        return $this->bitrate;
    }

    public function setBitrate(?AudioBitrate $bitrate): self
    {
        $this->bitrate = $bitrate;

        return $this;
    }

    /**
     * Indicate if album is released
     *
     * @return bool
     */
    public function isReleased(): bool
    {
        return $this->getReleasedAt() < new \DateTime();
    }

    public function serialize(){
        return [
            'id' => $this->getId(),
            'albumId' => $this->getAlbumId(),
            'isDownloaded' => $this->getIsDownloaded(),
        ];
    }

    public function getAddedAt(): ?\DateTimeImmutable
    {
        return $this->addedAt;
    }

    public function setAddedAt(\DateTimeImmutable $addedAt): self
    {
        $this->addedAt = $addedAt;

        return $this;
    }

    public function getDownloadedOn(): ?\DateTimeImmutable
    {
        return $this->downloadedOn;
    }

    public function setDownloadedOn(?\DateTimeImmutable $downloadedOn): self
    {
        $this->downloadedOn = $downloadedOn;

        return $this;
    }
}
