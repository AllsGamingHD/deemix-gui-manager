<?php

namespace App\Entity;

use App\Repository\ImportRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ImportRepository::class)
 */
class Import
{
    public const IMPORT_CONFIG = [
        1 => 'Fast import (Full artist & albums. Tracks not imported)',
        2 => 'Normal import (Full artist & albums/tracks not complete but imported)',
        3 => 'Deep import (Full artists, albums & tracks data)',
    ];
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $importValue;

    /**
     * @ORM\OneToMany(targetEntity=Artist::class, mappedBy="import", orphanRemoval=true)
     */
    private $artists;

    public function __construct()
    {
        $this->artists = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getImportValue(): ?string
    {
        return $this->importValue;
    }

    public function setImportValue(string $importValue): self
    {
        $this->importValue = $importValue;

        return $this;
    }

    public function __toString()
    {
        return $this->importValue;
    }

    /**
     * @return Collection|Artist[]
     */
    public function getArtists(): Collection
    {
        return $this->artists;
    }

    public function addArtist(Artist $artist): self
    {
        if (!$this->artists->contains($artist)) {
            $this->artists[] = $artist;
            $artist->setImport($this);
        }

        return $this;
    }

    public function removeArtist(Artist $artist): self
    {
        if ($this->artists->removeElement($artist)) {
            // set the owning side to null (unless already changed)
            if ($artist->getImport() === $this) {
                $artist->setImport(null);
            }
        }

        return $this;
    }
}
