<?php

namespace App\Entity;

use App\Repository\RecordTypeRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=RecordTypeRepository::class)
 */
class RecordType
{
    public const RECORD_TYPES = [
        'all'     => 'All (albums, ep\'s, singles, compiles)',
        'album'  => 'Only albums',
        'ep'   => 'Only ep\'s',
        'single' => 'Only singles',
        'compile' => 'Only compile',
    ];

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=10)
     */
    private $type;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $typeValue;

    /**
     * @ORM\OneToOne(targetEntity=Parameters::class, mappedBy="recordType", cascade={"persist", "remove"})
     */
    private $parameters;

    /**
     * @ORM\OneToMany(targetEntity=Artist::class, mappedBy="recordType", orphanRemoval=true)
     */
    private $artists;

    /**
     * @ORM\OneToMany(targetEntity=Album::class, mappedBy="recordType", orphanRemoval=true)
     */
    private $albums;

    public function __construct()
    {
        $this->artists = new ArrayCollection();
        $this->albums = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getTypeValue(): ?string
    {
        return $this->typeValue;
    }

    public function setTypeValue(string $typeValue): self
    {
        $this->typeValue = $typeValue;

        return $this;
    }

    /**
     * @return Collection|Artist[]
     */
    public function getArtists(): Collection
    {
        return $this->artists;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->getTypeValue() ?? '';
    }

    public function getParameters(): ?Parameters
    {
        return $this->parameters;
    }

    public function setParameters(Parameters $parameters): self
    {
        // set the owning side of the relation if necessary
        if ($parameters->getRecordType() !== $this) {
            $parameters->setRecordType($this);
        }

        $this->parameters = $parameters;

        return $this;
    }

    public function addArtist(Artist $artist): self
    {
        if (!$this->artists->contains($artist)) {
            $this->artists[] = $artist;
            $artist->setRecordType($this);
        }

        return $this;
    }

    public function removeArtist(Artist $artist): self
    {
        if ($this->artists->removeElement($artist)) {
            // set the owning side to null (unless already changed)
            if ($artist->getRecordType() === $this) {
                $artist->setRecordType(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Album[]
     */
    public function getAlbums(): Collection
    {
        return $this->albums;
    }

    public function addAlbum(Album $album): self
    {
        if (!$this->albums->contains($album)) {
            $this->albums[] = $album;
            $album->setRecordType($this);
        }

        return $this;
    }

    public function removeAlbum(Album $album): self
    {
        if ($this->albums->removeElement($album)) {
            // set the owning side to null (unless already changed)
            if ($album->getRecordType() === $this) {
                $album->setRecordType(null);
            }
        }

        return $this;
    }
}
