<?php

namespace App\Entity;

use App\Repository\ArtistRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use JetBrains\PhpStorm\ArrayShape;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * @ORM\Entity(repositoryClass=ArtistRepository::class)
 */
class Artist
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $artistId;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="text")
     */
    private $url;

    /**
     * @ORM\Column(type="integer")
     */
    private $nbAlbums;

    /**
     * @ORM\Column(type="text")
     */
    private $downloadPath;

    /**
     * @ORM\ManyToOne(targetEntity=RecordType::class, inversedBy="artists", cascade={"persist"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $recordType;

    /**
     * @ORM\Column(type="datetime_immutable")
     */
    private $startedMonitoringAt;

    /**
     * @ORM\ManyToOne(targetEntity=AudioBitrate::class, inversedBy="artists")
     * @ORM\JoinColumn(nullable=false)
     */
    private $bitrate;

    /**
     * @ORM\OneToMany(targetEntity=Track::class, mappedBy="artist", orphanRemoval=true)
     */
    private $tracks;

    /**
     * @ORM\OneToMany(targetEntity=Album::class, mappedBy="artist", orphanRemoval=true)
     */
    private $albums;

    /**
     * @ORM\Column(type="boolean")
     */
    private $alert;

    /**
     * @ORM\Column(type="text")
     */
    private $pictureUrl;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isFullyDownload = false;

    /**
     * @ORM\Column(type="boolean")
     */
    private $monitorActive = true;


    /**
     * @ORM\Column(type="datetime_immutable")
     */
    private $addedAt;

    /**
     * @ORM\Column(type="datetime_immutable", nullable=true)
     */
    private $downloadOn;

    /**
     * @ORM\ManyToOne(targetEntity=Import::class, inversedBy="artists")
     * @ORM\JoinColumn(nullable=false)
     */
    private $import;

    /**
     * @throws \Exception
     */
    public function __construct()
    {
        $this->startedMonitoringAt = new \DateTimeImmutable();
        $this->tracks              = new ArrayCollection();
        $this->albums              = new ArrayCollection();
        $this->setAddedAt(new \DateTimeImmutable());
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getArtistId(): ?int
    {
        return $this->artistId;
    }

    public function setArtistId(int $artistId): self
    {
        $this->artistId = $artistId;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getUrl(): ?string
    {
        return $this->url;
    }

    public function setUrl(string $url): self
    {
        $this->url = $url;

        return $this;
    }

    public function getNbAlbums(): ?int
    {
        return $this->nbAlbums;
    }

    public function setNbAlbums(int $nbAlbums): self
    {
        $this->nbAlbums = $nbAlbums;

        return $this;
    }

    public function getDownloadPath(): ?string
    {
        return $this->downloadPath;
    }

    public function setDownloadPath(string $downloadPath): self
    {
        $this->downloadPath = $downloadPath;

        return $this;
    }

    public function getRecordType(): ?RecordType
    {
        return $this->recordType;
    }

    public function setRecordType(?RecordType $recordType): self
    {
        $this->recordType = $recordType;

        return $this;
    }

    public function getStartedMonitoringAt(): ?\DateTimeImmutable
    {
        return $this->startedMonitoringAt;
    }

    public function setStartedMonitoringAt(\DateTimeImmutable $startedMonitoringAt): self
    {
        $this->startedMonitoringAt = $startedMonitoringAt;

        return $this;
    }

    public function getBitrate(): ?AudioBitrate
    {
        return $this->bitrate;
    }

    public function setBitrate(?AudioBitrate $bitrate): self
    {
        $this->bitrate = $bitrate;

        return $this;
    }

    /**
     * @return Collection|Track[]
     */
    public function getTracks(): Collection
    {
        return $this->tracks;
    }

    public function addTrack(Track $track): self
    {
        if (!$this->tracks->contains($track))
        {
            $this->tracks[] = $track;
            $track->setArtist($this);
        }

        return $this;
    }

    public function removeTrack(Track $track): self
    {
        if ($this->tracks->removeElement($track))
        {
            // set the owning side to null (unless already changed)
            if ($track->getArtist() === $this)
            {
                $track->setArtist(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Album[]
     */
    public function getAlbums(): Collection
    {
        return $this->albums;
    }

    public function addAlbum(Album $album): self
    {
        if (!$this->albums->contains($album))
        {
            $this->albums[] = $album;
            $album->setArtist($this);
        }

        return $this;
    }

    public function removeAlbum(Album $album): self
    {
        if ($this->albums->removeElement($album))
        {
            // set the owning side to null (unless already changed)
            if ($album->getArtist() === $this)
            {
                $album->setArtist(null);
            }
        }

        return $this;
    }

    public function getAlert(): ?bool
    {
        return $this->alert;
    }

    public function setAlert(bool $alert): self
    {
        $this->alert = $alert;

        return $this;
    }

    public function getPictureUrl(): ?string
    {
        return $this->pictureUrl;
    }

    public function setPictureUrl(string $pictureUrl): self
    {
        $this->pictureUrl = $pictureUrl;

        return $this;
    }

    public function getIsFullyDownload(): ?bool
    {
        return $this->isFullyDownload;
    }

    public function setIsFullyDownload(bool $isFullyDownload): self
    {
        $this->isFullyDownload = $isFullyDownload;

        return $this;
    }

    /**
     * @return array
     */
    #[ArrayShape(['download_path' => "null|string", 'bitrate' => "\App\Entity\AudioBitrate|null", 'record_type' => "\App\Entity\RecordType|null", 'alert' => "bool|null"])]
    public function getArtistImportConfiguration(): array
    {
        return [
            'download_path' => $this->getDownloadPath(),
            'bitrate'       => $this->getBitrate(),
            'record_type'   => $this->getRecordType(),
            'alert'         => $this->getAlert(),
        ];
    }

    public function getNbDownloadedAlbums()
    {
        $downloadedAlbumsCount = count($this->getAlbums()->filter(static function ($album, $key)
        {
            if ($album->getIsDownloaded() === 2)
            {
                return 1;
            }
        }));
        $totalAlbums           = $this->getAlbums()->count();
        return "$downloadedAlbumsCount/$totalAlbums";
    }

    public function serialize()
    {
        return [
            'id'                  => $this->getId(),
            'isFullyDownloaded'   => $this->getIsFullyDownload(),
            'artistId'            => $this->getArtistId(),
            'name'                => $this->getName(),
            'totalAlbums'         => $this->getNbAlbums(),
            'downloadedAlbums'    => $this->getNbDownloadedAlbums(),
            'downloadPath'        => $this->getDownloadPath(),
            'recordType'          => $this->getRecordType()->getTypeValue(),
            'bitrate'             => $this->getBitrate()->getBitrateName(),
            'alert'               => $this->getAlert(),
            'startedMonitoringAt' => $this->getStartedMonitoringAt()->format('Y-m-d H:i:s'),
            'albums'              => $this->getAlbums()->count(),
            'tracks'              => $this->getTracks()->count(),
        ];
    }

    public function getMonitorActive(): ?bool
    {
        return $this->monitorActive;
    }

    public function setMonitorActive(bool $monitorActive): self
    {
        $this->monitorActive = $monitorActive;

        return $this;
    }


    public function getAddedAt(): ?\DateTimeImmutable
    {
        return $this->addedAt;
    }

    public function setAddedAt(\DateTimeImmutable $addedAt): self
    {
        $this->addedAt = $addedAt;

        return $this;
    }

    public function getDownloadOn(): ?\DateTimeImmutable
    {
        return $this->downloadOn;
    }

    public function setDownloadOn(?\DateTimeImmutable $downloadOn): self
    {
        $this->downloadOn = $downloadOn;

        return $this;
    }

    public function getImport(): ?Import
    {
        return $this->import;
    }

    public function setImport(?Import $import): self
    {
        $this->import = $import;

        return $this;
    }
}
