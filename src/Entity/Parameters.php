<?php

namespace App\Entity;

use App\Repository\ParametersRepository;
use DateTime;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ParametersRepository::class)
 */
class Parameters
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="text")
     */
    private $downloadPath = 'C:\\Musique';

    /**
     * @ORM\Column(type="boolean")
     */
    private $alerting = false;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $smtpServer = '';

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $smtpPort = 465;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $smtpUser = '';

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $smtpPassword = '';

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $smtpEmailFrom = '';

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $smtpEmailTo = '';

    /**
     * @ORM\OneToOne(targetEntity=AudioBitrate::class, inversedBy="parameters", cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $bitrate;

    /**
     * @ORM\OneToOne(targetEntity=RecordType::class, inversedBy="parameters", cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $recordType;

    /**
     * @ORM\Column(type="text")
     */
    private $deemixPath = '';

    /**
     * @ORM\Column(type="text")
     */
    private $arl = '';

    /**
     * @ORM\Column(type="boolean")
     */
    private $checkReleaseByDate = '';

    /**
     * @ORM\Column(type="date")
     */
    private $checkReleaseByDateMaxDays;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $lastGlobalRefresh;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $lastImport;

    /**
     * @ORM\Column(type="boolean")
     */
    private $canDownload = false;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $timeZone = 'Europe/Paris';

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $deemixCommand = 'py -m deemix';

    /**
     * @ORM\Column(type="string", length=4)
     */
    private $language = 'en';

    /**
     * @ORM\ManyToOne(targetEntity=Import::class)
     * @ORM\JoinColumn(nullable=false)
     */
    private $deepDataGrap;

    public function __construct()
    {
        $this->checkReleaseByDateMaxDays = (new \DateTime())->sub(new \DateInterval('P90D'));
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDownloadPath(): ?string
    {
        return $this->downloadPath;
    }

    public function setDownloadPath(string $downloadPath): self
    {
        $this->downloadPath = $downloadPath;

        return $this;
    }

    public function getAlerting(): ?bool
    {
        return $this->alerting;
    }

    public function setAlerting(bool $alerting): self
    {
        $this->alerting = $alerting;

        return $this;
    }

    public function getSmtpServer(): ?string
    {
        return $this->smtpServer;
    }

    public function setSmtpServer(?string $smtpServer): self
    {
        $this->smtpServer = $smtpServer;

        return $this;
    }

    public function getSmtpPort(): ?int
    {
        return $this->smtpPort;
    }

    public function setSmtpPort(?int $smtpPort): self
    {
        $this->smtpPort = $smtpPort;

        return $this;
    }

    public function getSmtpUser(): ?string
    {
        return $this->smtpUser;
    }

    public function setSmtpUser(?string $smtpUser): self
    {
        $this->smtpUser = $smtpUser;

        return $this;
    }

    public function getSmtpPassword(): ?string
    {
        return $this->smtpPassword;
    }

    public function setSmtpPassword(?string $smtpPassword): self
    {
        $this->smtpPassword = $smtpPassword;

        return $this;
    }

    public function getSmtpEmailFrom(): ?string
    {
        return $this->smtpEmailFrom;
    }

    public function setSmtpEmailFrom(?string $smtpEmailFrom): self
    {
        $this->smtpEmailFrom = $smtpEmailFrom;

        return $this;
    }

    public function getSmtpEmailTo(): ?string
    {
        return $this->smtpEmailTo;
    }

    public function setSmtpEmailTo(?string $smtpEmailTo): self
    {
        $this->smtpEmailTo = $smtpEmailTo;

        return $this;
    }

    public function getBitrate(): ?AudioBitrate
    {
        return $this->bitrate;
    }

    public function setBitrate(AudioBitrate $bitrate): self
    {
        $this->bitrate = $bitrate;

        return $this;
    }

    public function getRecordType(): ?RecordType
    {
        return $this->recordType;
    }

    public function setRecordType(RecordType $recordType): self
    {
        $this->recordType = $recordType;

        return $this;
    }

    public function getDeemixPath(): ?string
    {
        return $this->deemixPath;
    }

    public function setDeemixPath(string $deemixPath): self
    {
        $this->deemixPath = $deemixPath;

        return $this;
    }

    public function getArl(): ?string
    {
        return $this->arl;
    }

    public function setArl(string $arl): self
    {
        $this->arl = $arl;

        return $this;
    }

    public function getCheckReleaseByDate(): ?bool
    {
        return $this->checkReleaseByDate;
    }

    public function setCheckReleaseByDate(bool $checkReleaseByDate): self
    {
        $this->checkReleaseByDate = $checkReleaseByDate;

        return $this;
    }

    public function getCheckReleaseByDateMaxDays(): ?DateTime
    {
        return $this->checkReleaseByDateMaxDays;
    }

    public function setCheckReleaseByDateMaxDays(DateTime $checkReleaseByDateMaxDays): self
    {
        $this->checkReleaseByDateMaxDays = $checkReleaseByDateMaxDays;

        return $this;
    }

    public function getLastGlobalRefresh(): ?\DateTimeInterface
    {
        return $this->lastGlobalRefresh;
    }

    public function setLastGlobalRefresh(\DateTimeInterface $lastGlobalRefresh): self
    {
        $this->lastGlobalRefresh = $lastGlobalRefresh;

        return $this;
    }

    public function getLastImport(): ?\DateTimeInterface
    {
        return $this->lastImport;
    }

    public function setLastImport(?\DateTimeInterface $lastImport): self
    {
        $this->lastImport = $lastImport;

        return $this;
    }


    public function getCanDownload(): ?bool
    {
        return $this->canDownload;
    }

    public function setCanDownload(bool $canDownload): self
    {
        $this->canDownload = $canDownload;

        return $this;
    }

    public function getTimeZone(): ?string
    {
        return $this->timeZone;
    }

    public function setTimeZone(string $timeZone): self
    {
        $this->timeZone = $timeZone;

        return $this;
    }

    public function getConfigurationArray()
    {
        return [
            'download_path' => $this->getDownloadPath(),
            'alert'         => $this->alerting,
            'record_type'   => $this->getRecordType()->getType(),
            'bitrate'       => $this->getBitrate()->getBitrateNumber(),
        ];
    }

    public function getDeemixCommand(): ?string
    {
        return $this->deemixCommand;
    }

    public function setDeemixCommand(string $deemixCommand): self
    {
        $this->deemixCommand = $deemixCommand;

        return $this;
    }

    public function getLanguage(): ?string
    {
        return $this->language;
    }

    public function setLanguage(string $language): self
    {
        $this->language = $language;

        return $this;
    }

    public function getDeepDataGrap(): ?Import
    {
        return $this->deepDataGrap;
    }

    public function setDeepDataGrap(?Import $deepDataGrap): self
    {
        $this->deepDataGrap = $deepDataGrap;

        return $this;
    }
}
