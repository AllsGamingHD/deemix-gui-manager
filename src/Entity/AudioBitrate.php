<?php

namespace App\Entity;

use App\Repository\AudioBitrateRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=AudioBitrateRepository::class)
 */
class AudioBitrate
{
    public const AUDIO_BITRATE = [
        1 => 'MP3 128 Kbps',
        3 => 'MP3 320 Kbps',
        9 => 'FLAC',
    ];

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $bitrateNumber;

    /**
     * @ORM\Column(type="string", length=10)
     */
    private $bitrateName;

    /**
     * @ORM\OneToOne(targetEntity=Parameters::class, mappedBy="bitrate", cascade={"persist", "remove"})
     */
    private $parameters;

    /**
     * @ORM\OneToMany(targetEntity=Artist::class, mappedBy="bitrate", orphanRemoval=true)
     */
    private $artists;

    /**
     * @ORM\OneToMany(targetEntity=Album::class, mappedBy="bitrate", orphanRemoval=true)
     */
    private $albums;

    public function __construct()
    {
        $this->artists = new ArrayCollection();
        $this->albums = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getBitrateNumber(): ?int
    {
        return $this->bitrateNumber;
    }

    public function setBitrateNumber(int $bitrateNumber): self
    {
        $this->bitrateNumber = $bitrateNumber;

        return $this;
    }

    public function getBitrateName(): ?string
    {
        return $this->bitrateName;
    }

    public function setBitrateName(string $bitrateName): self
    {
        $this->bitrateName = $bitrateName;

        return $this;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->getBitrateName() ?? '';
    }

    public function getParameters(): ?Parameters
    {
        return $this->parameters;
    }

    public function setParameters(Parameters $parameters): self
    {
        // set the owning side of the relation if necessary
        if ($parameters->getBitrate() !== $this) {
            $parameters->setBitrate($this);
        }

        $this->parameters = $parameters;

        return $this;
    }

    /**
     * @return Collection|Artist[]
     */
    public function getArtists(): Collection
    {
        return $this->artists;
    }

    public function addArtist(Artist $artist): self
    {
        if (!$this->artists->contains($artist)) {
            $this->artists[] = $artist;
            $artist->setBitrate($this);
        }

        return $this;
    }

    public function removeArtist(Artist $artist): self
    {
        if ($this->artists->removeElement($artist)) {
            // set the owning side to null (unless already changed)
            if ($artist->getBitrate() === $this) {
                $artist->setBitrate(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Album[]
     */
    public function getAlbums(): Collection
    {
        return $this->albums;
    }

    public function addAlbum(Album $album): self
    {
        if (!$this->albums->contains($album)) {
            $this->albums[] = $album;
            $album->setBitrate($this);
        }

        return $this;
    }

    public function removeAlbum(Album $album): self
    {
        if ($this->albums->removeElement($album)) {
            // set the owning side to null (unless already changed)
            if ($album->getBitrate() === $this) {
                $album->setBitrate(null);
            }
        }

        return $this;
    }
}
