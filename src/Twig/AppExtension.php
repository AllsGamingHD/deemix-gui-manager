<?php

namespace App\Twig;

use App\Entity\Album;
use App\Entity\Track;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;

class AppExtension extends AbstractExtension
{
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function getFilters()
    {
        return [
            new TwigFilter('getTracksAlbums', [$this, 'getTracksAlbums']),
            new TwigFilter('groupAlbumByYear', [$this, 'groupAlbumByYear']),
            new TwigFilter('groupTracksByYear', [$this, 'groupTracksByYear']),
        ];
    }

    /**
     * @param $data
     *
     * @return array
     */
    public function groupAlbumByYear($data): array
    {
        $result = [];
        foreach ($data as $element)
        {
            $result[$element->getReleasedAt()->format('Y')][ucfirst($element->getRecordType()->getType())][] = $element;
        }
        foreach ($result as $key => $res)
        {
            ksort($result[$key]);
        }
        return $result;
    }

    /**
     * @param $data
     *
     * @return array
     */
    public function getTracksAlbums($data): array
    {
        $result = [];
        foreach ($data as $element)
        {
            $tracks = $this->entityManager->getRepository(Track::class)->findBy(['isrc' => $element->getIsrc()]);
            foreach ($tracks as $track)
            {
                foreach ($track->getAlbum() as $album)
                {
                    $result[$element->getIsrc()][$album->getId()] = [
                        'name' => ($data->getOwner()->getId() === $album->getId() ? ' [CURRENT] ' : '') . $album->getName() . ' (' . ucfirst($album->getRecordType()->getType()) . ')', 'id' => $album->getId(),
                    ];
                }
            }
        }
        return $result;
    }

    /**
     * @param $data
     *
     * @return array
     */
    public function groupTracksByYear($data): array
    {
        $result = [];
        foreach ($data as $element)
        {
            $result[$element->getReleasedAt()->format('Y')][] = $element;
        }
        ksort($result);
        $result = array_reverse($result, true);
        return $result;
    }
}