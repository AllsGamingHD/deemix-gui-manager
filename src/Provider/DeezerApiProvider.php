<?php

namespace App\Provider;

use Psr\Log\LoggerInterface;

class DeezerApiProvider
{
    private const API_ERRORS   = [
        4   => 'QUOTA_LIMIT',
        100 => 'ITEM_LIMIT_EXCEEDED',
        200 => 'PERMISSION',
        300 => 'TOKEN_INVALID',
        500 => 'PARAMETER',
        501 => 'PARAMETER_MISSING',
        600 => 'QUERY_INVALID',
        700 => 'SERVICE_BUSY',
        800 => 'DATA_NOT_FOUND',
        900 => 'INDIVIDUAL_ACCOUNT_NOT_ALLOWED',
    ];
    private const API_BASE_URL = 'http://api.deezer.com/';

    private const MAX_REQUEST_PER_SECOND = 49;
    private static ?\DateTime $last_reset_datetime = null;
    private static ?\DateTime $current_date_time = null;
    private static int $nb_requested = 0;

    private static function handleRequestTime(): void
    {
        self::$nb_requested++;
        self::$current_date_time = new \DateTime();
        if (self::$last_reset_datetime === null)
        {
            self::$last_reset_datetime = new \DateTime();
        }

        $diffWithLastReset = self::$current_date_time->diff(self::$last_reset_datetime);

        if (self::$nb_requested >= self::MAX_REQUEST_PER_SECOND && $diffWithLastReset->s <= 5)
        {
            $awaitTime = 5 - $diffWithLastReset->s;
            $awaitTime = $awaitTime <= 0 ? 1 : $awaitTime;
            sleep($awaitTime);
            self::$last_reset_datetime = new \DateTime();
            self::$nb_requested        = 0;
        }
    }

    /**
     * @throws \JsonException
     * @throws \Exception
     */
    private static function callApi(string $endpoint): array
    {
        self::handleRequestTime();
        $ch   = curl_init();
        $char = '?';
        if (str_contains($endpoint, '?'))
        {
            $char = '&';
        }
        $url = self::API_BASE_URL . $endpoint . $char . 'limit=10000';
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HEADER, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $response    = curl_exec($ch);
        $header_size = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
        $body        = substr($response, $header_size);

        $resCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        if ($resCode === 200)
        {
            $body = json_decode($body, true, 512, JSON_THROW_ON_ERROR);
            if (isset($body['error']))
            {
                if ($body['error']['code'] === 4)
                {
                    sleep(1);
                    return self::callApi($endpoint);
                }

                self::handleError($body['error']);
            }
            return $body['data'] ?? $body;
        }

        throw new \Exception('An error is occurred on request error code : ' . $resCode . ' - ' . curl_error($ch));
    }

    /**
     * API Response error handling
     *
     * @param array $error
     *
     * @throws \Exception
     */
    private static function handleError(array $error): void
    {
        var_dump($error['code']);
        if (array_key_exists($error['code'], self::API_ERRORS))
        {
            throw new \Exception('Deezer API ERROR : ' . self::API_ERRORS[$error['code']] . ' - message: ' . $error['message']);
        }
        else
        {
            throw new \Exception('Error when calling Deezer API, unknown Exception');
        }
    }

    /**
     * Get information about artist by her ID
     *
     * @param int $id
     *
     * @return array
     * @throws \JsonException
     */
    public function getArtistById(int $id): array
    {
        return self::callApi("/artist/$id");
    }

    public function getAlbumById(int $albumId): array
    {
        return self::callApi("/album/$albumId");
    }

    /**
     * Get all albums released of artist
     *
     * @param int $artistId
     *
     * @return array
     * @throws \JsonException
     */
    public function getArtistAlbumsById(int $artistId): array
    {
        return self::callApi("/artist/$artistId/albums");
    }

    /**
     * Get all tracks of albums by her ID
     *
     * @param int $artistId
     *
     * @return array
     * @throws \JsonException
     */
    public function getAlbumTracks(int $albumId): array
    {
        return self::callApi("/album/$albumId/tracks");
    }

    /**
     * Get information about track by her ID
     *
     * @param int $trackId
     *
     * @return array
     * @throws \JsonException
     */
    public function getTrackById(int $trackId): array
    {
        return self::callApi("/track/$trackId");
    }

    /**
     * Get playlist information by her ID
     *
     * @param int $playlistId
     *
     * @return array
     * @throws \JsonException
     */
    public function getPlaylistById(int $playlistId): array
    {
        return self::callApi("/playlist/$playlistId");
    }

    /**
     * Get playlist trackslist information by her ID
     *
     * @param int $playlistId
     *
     * @return array
     * @throws \JsonException
     */
    public function getPlaylistTracklistById(int $playlistId): array
    {
        return self::callApi("/playlist/$playlistId/tracks");
    }

    /**
     * Query Deezer API
     *
     * @param string $query
     *
     * @return array
     * @throws \JsonException
     */
    public function search(string $query): array
    {
        return self::callApi("/search$query");
    }
}