<?php

namespace App\Message;

use App\Controller\AlbumCrudController;
use App\Controller\ArtistCrudController;
use App\Entity\Album;
use App\Entity\Parameters;
use App\Helper\GuiUpdater;
use App\Helper\NotifierHelper;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

class DeemixStartSingleDownloadHandler implements MessageHandlerInterface
{
    private ?EntityManagerInterface $entityManager;
    private ?NotifierHelper $notifierHelper;
    private ?LoggerInterface $logger;
    private ?GuiUpdater $guiUpdater;

    public function __construct(EntityManagerInterface $entityManager, NotifierHelper $notifierHelper, LoggerInterface $logger, GuiUpdater $guiUpdater)
    {
        $this->entityManager  = $entityManager;
        $this->notifierHelper = $notifierHelper;
        $this->logger         = $logger;
        $this->guiUpdater     = $guiUpdater;
    }

    public function __invoke(DeemixStartSingleDownload $deemixStarter)
    {
        /** @var Parameters $parameters */
        $parameters = $this->entityManager->getRepository(Parameters::class)->findLast();
        /** @var Album $album */
        $album = $this->entityManager->getRepository(Album::class)->findOneBy(['albumId' => $deemixStarter->getAlbumId()]);


        $this->entityManager->getRepository(Album::class)->setIsDownloadingById($album->getAlbumId());
        $serialize = [
            'id' => $album->getId(),
            'albumId' => $album->getAlbumId(),
            'isDownloaded' => 1,
        ];
        $this->guiUpdater->update(ArtistCrudController::class, 'updateAlb', $serialize);
        $this->guiUpdater->update(AlbumCrudController::class, 'update', $serialize);

        $res = shell_exec("{$parameters->getDeemixCommand()} -b {$album->getArtist()->getBitrate()->getBitrateNumber()} -p {$album->getArtist()->getDownloadPath()} {$album->getUrl()}");

        $this->entityManager->getRepository(Album::class)->setIsDownloadedById($album->getAlbumId());
        $serialize = [
            'id' => $album->getId(),
            'albumId' => $album->getAlbumId(),
            'isDownloaded' => 2,
        ];
        $this->guiUpdater->update(ArtistCrudController::class, 'updateAlb', $serialize);
        $this->guiUpdater->update(AlbumCrudController::class, 'update', $serialize);
        $this->entityManager->getRepository(Album::class)->updateArtistIfAllDownloaded($album->getArtist()->getId());
    }
}