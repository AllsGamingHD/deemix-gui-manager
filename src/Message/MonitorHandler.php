<?php

namespace App\Message;

use App\Entity\Parameters;
use App\Helper\NotifierHelper;
use App\Manager\ImportationManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpKernel\KernelInterface;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

class MonitorHandler implements MessageHandlerInterface
{
    private $importationManager;
    private $notifierHelper;

    public function __construct(ImportationManager $importationManager, NotifierHelper $notifierHelper)
    {
        $this->importationManager = $importationManager;
        $this->notifierHelper     = $notifierHelper;
    }

    public function __invoke(Monitor $monitor)
    {
        $this->notifierHelper->setIcon('info')->setContent('Request received', "Successfully received import request");
        if ($monitor->getType() && $monitor->getId())
        {
            if ($monitor->getType() === 'artist')
            {
                $this->importationManager->importArtist(['artistId' => $monitor->getId()]);
            }
            else if ($monitor->getType() === 'playlist')
            {
                $this->importationManager->importArtistFromPlaylist($monitor->getId(), $monitor->getOpt());
            }
        }
    }
}