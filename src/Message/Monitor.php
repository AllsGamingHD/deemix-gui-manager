<?php

namespace App\Message;

class Monitor
{
    private string $type;
    private int $id;
    private ?string $opt;

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @param string $type
     *
     * @return Monitor
     */
    public function setType(string $type): Monitor
    {
        $this->type = $type;
        return $this;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     *
     * @return Monitor
     */
    public function setId(int $id): Monitor
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getOpt(): string
    {
        return $this->opt;
    }

    /**
     * @param string|null $opt
     *
     * @return Monitor
     */
    public function setOpt(?string $opt): Monitor
    {
        $this->opt = $opt;
        return $this;
    }
}