<?php

namespace App\Message;

use App\Controller\AlbumCrudController;
use App\Controller\ArtistCrudController;
use App\Entity\Album;
use App\Entity\Artist;
use App\Entity\Parameters;
use App\Helper\GuiUpdater;
use App\Helper\NotifierHelper;
use App\Manager\DownloaderQueueManager;
use App\Manager\ImportationManager;
use App\Manager\RefreshManager;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Mercure\Update;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Process\Process;
use function Symfony\Component\Translation\t;

class DeemixStarterHandler implements MessageHandlerInterface
{
    private ?EntityManagerInterface $entityManager;
    private ?NotifierHelper $notifierHelper;
    private ?LoggerInterface $logger;
    private ?GuiUpdater $guiUpdater;
    private ?DownloaderQueueManager $queueManager;

    public function __construct(EntityManagerInterface $entityManager, NotifierHelper $notifierHelper, LoggerInterface $logger, GuiUpdater $guiUpdater, DownloaderQueueManager $queueManager)
    {
        $this->entityManager  = $entityManager;
        $this->notifierHelper = $notifierHelper;
        $this->logger         = $logger;
        $this->guiUpdater     = $guiUpdater;
        $this->queueManager   = $queueManager;
        ini_set('memory_limit', '2G');
    }

    public function __invoke(DeemixStarter $deemixStarter)
    {
        $albumToDownload = $this->entityManager->getRepository(Album::class)->getNotDownloaded();
        foreach ($albumToDownload as $album)
        {
            $this->queueManager->addToQueue($album);
        }
        $this->queueManager->startQueue();
        /*if ($albumToDownload)
        {
            $progress = 0;
            $total    = count($albumToDownload);
            $this->notifierHelper->setContent('Start task of downloading ' . $total . ' albums')->notify();
            $inDownload       = [];
            $downloadProgress = 0;
            while ($downloadProgress !== $total)
            {
                while (count($inDownload) < 15)
                {
                    $album     = $albumToDownload[$downloadProgress++];
                    $serialize = [
                        'id'           => $album->getId(),
                        'albumId'      => $album->getAlbumId(),
                        'isDownloaded' => 1,
                    ];
                    $this->guiUpdater->update(ArtistCrudController::class, 'updateAlb', $serialize);
                    $this->guiUpdater->update(AlbumCrudController::class, 'update', $serialize);
                    $this->entityManager->getRepository(Album::class)->setIsDownloadingById($album->getAlbumId());
                    $this->notifierHelper->setContent('Start downloading : ' . $album->getName() . '.')->notify();
                    $this->logger->info("Start downloading album [{$album->getAlbumId()}] {$album->getName()}");

                    $process = Process::fromShellCommandline("py -m deemix -b {$album->getArtist()->getBitrate()->getBitrateNumber()} -p {$album->getArtist()->getDownloadPath()} {$album->getUrl()}");
                    $process->setTimeout(600);
                    $process->setIdleTimeout(600);
                    $process->start();
                    $inDownload[] = [
                        'process'  => $process,
                        'albumId'  => $album->getAlbumId(),
                        'artistId' => $album->getArtist()->getId(),
                    ];
                }
                while (count($inDownload) !== 0)
                {
                    foreach ($inDownload as $key => $download)
                    {
                        if (!$download['process']->isRunning())
                        {
                            $this->logger->info("[{$download['albumId']}] Downloaded");
                            $this->entityManager->getRepository(Album::class)->setIsDownloadedById($download['albumId']);
                            unset($inDownload[$key]);
                            $isFullyDownloaded = $this->entityManager->getRepository(Album::class)
                                                                     ->updateArtistIfAllDownloaded($download['artistId']);
                            $serialize         = [
                                'id'           => $download['albumId'],
                                'albumId'      => $download['artistId'],
                                'isDownloaded' => 2,
                            ];
                            $this->guiUpdater->update(ArtistCrudController::class, 'updateAlb', $serialize);
                            $this->guiUpdater->update(AlbumCrudController::class, 'update', $serialize);

                            $this->guiUpdater->update(ArtistCrudController::class, 'update', [
                                'artistId' => $album->getArtist()
                                                    ->getId(), 'isFullyDownloaded' => $isFullyDownloaded, 'totalAlbums' => $album->getArtist()
                                                                                                                                 ->getAlbums()
                                                                                                                                 ->count(),
                            ]);
                        }
                    }
                    sleep(1);
                }
            }*/
        /* foreach ($albumToDownload as $album)
         {

             if ($res === 'Paste here your arl:')
             {
                 if (!is_dir($parameters->getDeemixPath()))
                 {
                     $this->notifierHelper->setIcon('error')->setContent('Please set a valid deemix path in your parameters !')->notify();
                     return;
                 }
                 else
                 {
                     file_put_contents('.arl', $parameters->getArl());
                     $res = shell_exec("{$parameters->getDeemixCommand()} -b {$album->getArtist()->getBitrate()->getBitrateNumber()} -p {$album->getArtist()->getDownloadPath()} {$album->getUrl()}");
                 }
             }
             $this->notifierHelper->setContent('Successfully downloaded : ' . $album->getName() . '.')->notify();

             */
        // }
    }
}