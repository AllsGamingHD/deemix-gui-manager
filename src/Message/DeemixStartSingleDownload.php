<?php

namespace App\Message;

class DeemixStartSingleDownload
{
    private int $albumId;

    /**
     * @return int
     */
    public function getAlbumId(): int
    {
        return $this->albumId;
    }

    /**
     * @param int $albumId
     * @return DeemixStartSingleDownload
     */
    public function setAlbumId(int $albumId): DeemixStartSingleDownload
    {
        $this->albumId = $albumId;
        return $this;
    }
}