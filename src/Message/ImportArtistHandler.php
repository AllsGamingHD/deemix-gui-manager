<?php

namespace App\Message;

use App\Manager\ImportationManager;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

class ImportArtistHandler implements MessageHandlerInterface
{
    private ?ImportationManager $importationManager;

    public function __construct(ImportationManager $importationManager)
    {
        $this->importationManager = $importationManager;
    }

    public function __invoke(ImportArtist $importArtist)
    {
        $this->importationManager->importArtist($importArtist->getData());
    }
}