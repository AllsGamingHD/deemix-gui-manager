<?php

namespace App\Message;

use App\Manager\ImportationManager;
use App\Manager\RefreshManager;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

class RefreshArtistHandler implements MessageHandlerInterface
{
    private ?RefreshManager $refreshManager;

    public function __construct(RefreshManager $refreshManager)
    {
        $this->refreshManager = $refreshManager;
    }

    public function __invoke(RefreshArtist $refreshArtist)
    {
        $this->refreshManager->refreshArtist();
    }
}