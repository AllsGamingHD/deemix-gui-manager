<?php

namespace App\Repository;

use App\Entity\Artist;
use DateTime;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Artist|null find($id, $lockMode = null, $lockVersion = null)
 * @method Artist|null findOneBy(array $criteria, array $orderBy = null)
 * @method Artist[]    findAll()
 * @method Artist[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ArtistRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Artist::class);
    }

    /**
     * Return all id's of artists
     *
     * @return array
     */
    public function getAllArtistsId(): array
    {
        $artists = $this->createQueryBuilder('a')->select('a.artistId')->getQuery()->getArrayResult();
        return $artists !== null ? array_column($artists, 'artistId') : [];
    }

    public function getNotMonitoredActive()
    {
        return $this->createQueryBuilder('a')->select('a')->where('a.monitorActive = 1')->getQuery()->getResult();
    }

    public function getLastXMinutesMonitoring(int $splitTime = 86400, DateTime $startDate = null, DateTime $endDate = null)
    {
        $pdo  = $this->getEntityManager()->getConnection();
        $stmt = $pdo->prepare("SELECT strftime('%Y-%m-%d %H:%M:00', datetime((strftime('%s', added_at) / " . $splitTime . ") * " . $splitTime . ", 'unixepoch')) AS `datetime`,
                                           strftime('%Y-%m-%d', datetime(added_at, 'localtime'))       AS `date`,
                                           strftime('%H:00', datetime(added_at, 'localtime'))          AS `time`,
                                           COUNT(added_at)                                             AS `total` 
                                    FROM artist 
                                    WHERE added_at >= :startDate
                                      AND added_at <= :endDate GROUP BY datetime ORDER BY datetime");
        $stmt->bindValue(':startDate', $startDate->format('Y-m-d H:i:s'));
        $stmt->bindValue(':endDate', $endDate->format('Y-m-d H:i:s'));
        $stmt->executeStatement();
        return $stmt->fetchAll();
    }

    public function getLastXMinutesDownloaded(int $splitTime = 86400, DateTime $startDate = null, DateTime $endDate = null)
    {
        $pdo  = $this->getEntityManager()->getConnection();
        $stmt = $pdo->prepare("SELECT strftime('%Y-%m-%d %H:%M:00', datetime((strftime('%s', download_on) / " . $splitTime . ") * " . $splitTime . ", 'unixepoch')) AS `datetime`,
                                           strftime('%Y-%m-%d', datetime(download_on, 'localtime'))       AS `date`,
                                           strftime('%H:00', datetime(download_on, 'localtime'))          AS `time`,
                                           COUNT(download_on)                                             AS `total` 
                                    FROM artist 
                                    WHERE download_on >= :startDate
                                      AND download_on <= :endDate GROUP BY datetime ORDER BY datetime");
        $stmt->bindValue(':startDate', $startDate->format('Y-m-d H:i:s'));
        $stmt->bindValue(':endDate', $endDate->format('Y-m-d H:i:s'));
        $stmt->executeStatement();
        return $stmt->fetchAll();
    }

    public function getTotalCountAndTotalDownloaded()
    {
        $pdo  = $this->getEntityManager()->getConnection();
        $stmt = $pdo->prepare("SELECT (SELECT COUNT(id) FROM artist) AS total, (SELECT COUNT(id) FROM artist WHERE is_fully_download = 1)  AS totalDownloaded");
        $stmt->executeStatement();
        return $stmt->fetch();
    }
    // /**
    //  * @return Artist[] Returns an array of Artist objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a . exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('a . id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Artist
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a . exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
