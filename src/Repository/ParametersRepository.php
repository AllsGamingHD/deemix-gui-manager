<?php

namespace App\Repository;

use App\Entity\Parameters;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\Validator\Constraints\Date;

/**
 * @method Parameters|null find($id, $lockMode = null, $lockVersion = null)
 * @method Parameters|null findOneBy(array $criteria, array $orderBy = null)
 * @method Parameters[]    findAll()
 * @method Parameters[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ParametersRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Parameters::class);
    }

    public function findLast()
    {
        return $this->createQueryBuilder('p')->select('p')->orderBy('p.id', 'DESC')->setMaxResults(1)->getQuery()->getOneOrNullResult();
    }

    public function updateLastImportToNow(): int
    {
        $last = $this->findLast();
        return $this->createQueryBuilder('p')->update('App:Parameters', 'p')->where('p.id = :id')->set('p.lastImport', ':date')
                    ->setParameter('id', $last->getId())->setParameter('date', (new \DateTime())->format('Y-m-d H:i:s'))->getQuery()->execute();
    }

    public function updateLastRefreshToNow(): int
    {
        $last = $this->findLast();
        return $this->createQueryBuilder('p')->update('App:Parameters', 'p')->where('p.id = :id')->set('p.lastGlobalRefresh', ':date')
                    ->setParameter('id', $last->getId())->setParameter('date', (new \DateTime('now', new \DateTimeZone('Europe/Paris')))->format('Y-m-d H:i:s'))->getQuery()->execute();
    }

    // /**
    //  * @return Parameters[] Returns an array of Parameters objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Parameters
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
