<?php

namespace App\Repository;

use App\Entity\Album;
use DateTime;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Album|null find($id, $lockMode = null, $lockVersion = null)
 * @method Album|null findOneBy(array $criteria, array $orderBy = null)
 * @method Album[]    findAll()
 * @method Album[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AlbumRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Album::class);
    }

    public function findAllAsArray()
    {
        return $this->createQueryBuilder('a')->select('a')->getQuery()->getArrayResult();
    }

    public function getDownloaded()
    {
        return $this->createQueryBuilder('a')->select('a')->where('a.isDownloaded = 2');
    }

    public function getNotDownloaded()
    {
        return $this->createQueryBuilder('a')
                    ->innerJoin('a.artist', 'artist')
                    ->select('a')
                    ->where('a.isDownloaded <> 2 AND a.isDownloaded <> 3 AND a.releasedAt <= :date')
                    ->setParameter('date', new \DateTime('now', new \DateTimeZone('Europe/Paris')))
                    ->getQuery()->getResult();
    }

    /**
     * @param int $albumId
     *
     * @return int|mixed|string
     * @throws \Doctrine\DBAL\Driver\Exception
     * @throws \Doctrine\DBAL\Exception
     */
    public function setIsDownloadedById(int $albumId): mixed
    {
        $conn = $this->getEntityManager()->getConnection();
        $stmt = $conn->prepare('UPDATE album SET is_downloaded = 2, downloaded_on = datetime("now", "localtime") WHERE album_id = :albumId AND date() >= released_at');
        $stmt->bindValue(':albumId', $albumId);
        return $stmt->execute();
    }

    /**
     * @param int $albumId
     *
     * @return int|mixed|string
     * @throws \Doctrine\DBAL\Driver\Exception
     * @throws \Doctrine\DBAL\Exception
     */
    public function setDownloadedFailedById(int $albumId): mixed
    {
        $conn = $this->getEntityManager()->getConnection();
        $stmt = $conn->prepare('UPDATE album SET is_downloaded = 3 WHERE album_id = :albumId AND date() >= released_at');
        $stmt->bindValue(':albumId', $albumId);
        return $stmt->execute();
    }

    /**
     * @param int $albumId
     *
     * @return int|mixed|string
     * @throws \Doctrine\DBAL\Driver\Exception
     * @throws \Doctrine\DBAL\Exception
     */
    public function setIsDownloadingById(int $albumId): mixed
    {
        $conn = $this->getEntityManager()->getConnection();
        $stmt = $conn->prepare('UPDATE album SET is_downloaded = 1 WHERE album_id = :albumId AND date() >= released_at');
        $stmt->bindValue(':albumId', $albumId);
        return $stmt->execute();
    }

    /**
     * @param int $id artist ID
     *
     * @throws \Doctrine\DBAL\Driver\Exception
     * @throws \Doctrine\DBAL\Exception
     */
    public function updateArtistIfAllDownloaded(int $id): bool
    {
        $conn = $this->getEntityManager()->getConnection();

        $stmt = $conn->prepare('SELECT CASE
                                            WHEN EXISTS(
                                                    SELECT 1
                                                    FROM album
                                                    WHERE album.artist_id = :artistId AND is_downloaded <> 2 AND is_downloaded <> 3
                                                ) THEN 0
                                            ELSE 1
                                            END AS `isFullyDownloaded`');
        $stmt->bindValue(':artistId', $id);
        $stmt->executeQuery();
        $isAllDownloaded = !(bool)$stmt->fetch()['isFullyDownloaded'];
        if ($isAllDownloaded)
        {
            $stmt = $conn->prepare('UPDATE artist SET is_fully_download = 1, download_on = datetime("now", "localtime") WHERE artist.id = :artistId');
            $stmt->bindValue(':artistId', $id);
            return $stmt->execute();
        }
        return false;
    }

    public function getDownloadedAlbumNumberByArtistId(int $artistId)
    {
        return $this->createQueryBuilder('a')->select('COUNT(a.id) AS downloaded')->where('a.artist = :artistId')
                    ->setParameter('artistId', $artistId);
    }

    /**
     * @param array $albumIds
     *
     * @return mixed
     */
    public function getAlbumsExisting(array $albumIds): mixed
    {
        return $this->createQueryBuilder('a')->select('a.albumId')->where('a.albumId IN (:ids)')->setParameter('ids', $albumIds)->getQuery()
                    ->getResult();
    }

    public function getLastXMinutesMonitoring(int $splitTime = 86400, DateTime $startDate = null, DateTime $endDate = null)
    {
        $pdo  = $this->getEntityManager()->getConnection();
        $stmt = $pdo->prepare("SELECT strftime('%Y-%m-%d %H:%M:00', datetime((strftime('%s', added_at) / " . $splitTime . ") * " . $splitTime . ", 'unixepoch')) AS `datetime`,
                                           strftime('%Y-%m-%d', datetime(added_at, 'localtime'))       AS `date`,
                                           strftime('%H:00', datetime(added_at, 'localtime'))          AS `time`,
                                           COUNT(added_at)                                             AS `total` 
                                    FROM album 
                                    WHERE added_at >= :startDate
                                      AND added_at <= :endDate GROUP BY datetime ORDER BY datetime");
        $stmt->bindValue(':startDate', $startDate->format('Y-m-d H:i:s'));
        $stmt->bindValue(':endDate', $endDate->format('Y-m-d H:i:s'));
        $stmt->executeStatement();
        return $stmt->fetchAll();
    }

    public function getLastXMinutesDownloaded(int $splitTime = 86400, DateTime $startDate = null, DateTime $endDate = null)
    {
        $pdo  = $this->getEntityManager()->getConnection();
        $stmt = $pdo->prepare("SELECT strftime('%Y-%m-%d %H:%M:00', datetime((strftime('%s', downloaded_on) / " . $splitTime . ") * " . $splitTime . ", 'unixepoch')) AS `datetime`,
                                           strftime('%Y-%m-%d', datetime(downloaded_on, 'localtime'))       AS `date`,
                                           strftime('%H:00', datetime(downloaded_on, 'localtime'))          AS `time`,
                                           COUNT(downloaded_on)                                             AS `total` 
                                    FROM album 
                                    WHERE downloaded_on >= :startDate
                                      AND downloaded_on <= :endDate GROUP BY datetime ORDER BY datetime");
        $stmt->bindValue(':startDate', $startDate->format('Y-m-d H:i:s'));
        $stmt->bindValue(':endDate', $endDate->format('Y-m-d H:i:s'));
        $stmt->executeStatement();
        return $stmt->fetchAll();
    }

    public function getAlbumReleasedAtMonitor(int $yearStart = 1900, int $yearEnd = 2100)
    {
        $pdo  = $this->getEntityManager()->getConnection();
        $stmt = $pdo->prepare("SELECT COUNT(id) AS `total`, strftime('%Y', released_at) AS `released_at` FROM album WHERE released_at >= :yearStart AND released_at <= :yearEnd GROUP BY strftime('%Y', released_at) ORDER BY released_at ASC");
        $stmt->bindValue(':yearStart', "$yearStart-01-01");
        $stmt->bindValue(':yearEnd', "$yearEnd-31-12");
        $stmt->executeStatement();
        return $stmt->fetchAll();
    }

    public function getTotalCountAndTotalDownloaded()
    {
        $pdo  = $this->getEntityManager()->getConnection();
        $stmt = $pdo->prepare("SELECT (SELECT COUNT(id) FROM album) AS total, (SELECT COUNT(id) FROM album WHERE is_downloaded = 2) AS totalDownloaded, (SELECT COUNT(id) FROM album WHERE is_downloaded = 1) AS totalInDownload, (SELECT COUNT(id) FROM album WHERE is_downloaded = 3) AS totalFailedDownload, SUM((SELECT COUNT(id) FROM album) - (SELECT COUNT(id) FROM album WHERE is_downloaded = 3)) AS totalWithNotFailed");
        $stmt->executeStatement();
        return $stmt->fetch();
    }

    // /**
    //  * @return Album[] Returns an array of Album objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('a.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Album
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
