<?php

namespace App\Repository;

use App\Entity\AudioBitrate;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method AudioBitrate|null find($id, $lockMode = null, $lockVersion = null)
 * @method AudioBitrate|null findOneBy(array $criteria, array $orderBy = null)
 * @method AudioBitrate[]    findAll()
 * @method AudioBitrate[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AudioBitrateRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, AudioBitrate::class);
    }

    // /**
    //  * @return AudioBitrate[] Returns an array of AudioBitrate objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('a.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?AudioBitrate
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
