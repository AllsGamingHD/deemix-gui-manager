<?php

namespace App\Repository;

use App\Entity\Track;
use DateTime;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Track|null find($id, $lockMode = null, $lockVersion = null)
 * @method Track|null findOneBy(array $criteria, array $orderBy = null)
 * @method Track[]    findAll()
 * @method Track[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TrackRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Track::class);
    }

    /**
     * @param array $trackIds
     *
     * @return mixed
     */
    public function getTracksExisting(array $trackIds): mixed
    {
        return $this->createQueryBuilder('t')->select('t.trackId')->where('t.trackId IN (:ids)')->setParameter('ids', $trackIds)->getQuery()
                    ->getResult();
    }

    public function getLastXMinutesMonitoring(int $splitTime = 86400, DateTime $startDate = null, DateTime $endDate = null)
    {
        $pdo  = $this->getEntityManager()->getConnection();
        $stmt = $pdo->prepare("SELECT strftime('%Y-%m-%d %H:%M:00', datetime((strftime('%s', added_at) / " . $splitTime . ") * " . $splitTime . ", 'unixepoch')) AS `datetime`,
                                           strftime('%Y-%m-%d', datetime(added_at, 'localtime'))       AS `date`,
                                           strftime('%H:00', datetime(added_at, 'localtime'))          AS `time`,
                                           COUNT(added_at)                                             AS `total` 
                                    FROM track 
                                    WHERE added_at >= :startDate
                                      AND added_at <= :endDate GROUP BY datetime ORDER BY datetime");
        $stmt->bindValue(':startDate', $startDate->format('Y-m-d H:i:s'));
        $stmt->bindValue(':endDate', $endDate->format('Y-m-d H:i:s'));
        $stmt->executeStatement();
        return $stmt->fetchAll();
    }

    // /**
    //  * @return Track[] Returns an array of Track objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('t.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Track
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
