<?php

namespace App\Trait;

trait ImporterTrait
{
    private array $options = [];

    private function excludeColumns(array $data): array
    {
        foreach ($data as $index => $array)
        {
            foreach ($this->options['excludedColumns'] as $excludedColumnName)
            {
                if (array_key_exists($excludedColumnName, $array))
                {
                    unset($data[$index][$excludedColumnName]);
                }
            }
        }
        return $data;
    }
}