<?php

namespace App\DataFixtures;

use App\Entity\AudioBitrate;
use App\Entity\Import;
use App\Entity\Parameters;
use App\Entity\RecordType;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class ParametersFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $parameters = new Parameters();
        $parameters->setBitrate($manager->getRepository(AudioBitrate::class)->findOneBy(['bitrateNumber' => '3']));
        $parameters->setRecordType($manager->getRepository(RecordType::class)->findOneBy(['type' => 'all']));
        $parameters->setDeepDataGrap($manager->getRepository(Import::class)->findOneBy(['id' => 1]));
        $manager->persist($parameters);
        $manager->flush();
    }

    public function getDependencies()
    {
        return [AudioBitrateFixtures::class, RecordTypeFixtures::class, ImportFixtures::class];
    }
}
