<?php

namespace App\DataFixtures;

use App\Entity\AudioBitrate;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class AudioBitrateFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        foreach (AudioBitrate::AUDIO_BITRATE as $bitrateNum => $bitrateValue)
        {
            $bitrate = new AudioBitrate();
            $bitrate->setBitrateName($bitrateValue)->setBitrateNumber($bitrateNum);
            $manager->persist($bitrate);
        }
        $manager->flush();
    }
}
