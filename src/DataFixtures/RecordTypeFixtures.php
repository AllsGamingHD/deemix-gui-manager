<?php

namespace App\DataFixtures;

use App\Entity\RecordType;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class RecordTypeFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        foreach (RecordType::RECORD_TYPES as $recordTypeKey => $recordTypeValue)
        {
            $bitrate = new RecordType();
            $bitrate->setType($recordTypeKey)->setTypeValue($recordTypeValue);
            $manager->persist($bitrate);
        }
        $manager->flush();
    }
}
