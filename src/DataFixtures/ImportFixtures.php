<?php

namespace App\DataFixtures;

use App\Entity\AudioBitrate;
use App\Entity\Import;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class ImportFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        foreach (Import::IMPORT_CONFIG as $importValue)
        {
            $import = new Import();
            $import->setImportValue($importValue);
            $manager->persist($import);
        }
        $manager->flush();
    }
}
