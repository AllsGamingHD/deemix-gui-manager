# deemix-gui-manager

## Requirements & Installation


### - Requirements :
* PHP >= 8.0.0
* Composer
* Use [Symfony CLI](https://symfony.com/download)

#### /!\ Attention
You need to add php to your environnement variable to use `php bin/console` command
[Follow this guide](https://devdojo.com/tnylea/installing-php-on-windows) just get PHP >=8.0.0 instead of 7.2

-----
#### When you are ready and have installed all necessary please follow this step (only first time) :
    1. execute commande `composer install`
    2. execute command `symfony serve` (for start the server, see *)
    3. Ensure data.db/messenger.db files is not existing in "var" directory, ensure the directories migrations is empty too
    3. execute command `php bin/console make:migration` (create migration files to iniate db)
    4. execute command `php bin/console doctrine:migrations:migrate` (apply migrations)
    5. execute command `php bin/console doctrine:fixtures:load` (load default neccesary data)
    6. execute command `php bin/console translation:pull --force` (getting translation from online API [require token, contact me for get one])
    7. execute command `php bin/console messenger:consume async -vv` (need to add, download artist principaly. used for others action please sure this command runned !)
### Here WE GOO you can use tools ! !
-----
#### When you have already followed step the first time, the second times you will only need to execute theses command to access the GUI and his functionnalities
    1. execute command `symfony serve` (for start the server, see *)
    2. execute command `php bin/console messenger:consume async -vv` (need to add, download artist principaly. used for others action please sure this command runned !)
-----
### * Generally server will start on port 8000, but sometimes if already used port can be changed to 8001.  
> When symfony serve launched, it will return a localhost ip with port. Please check if this port is the same as configured in .env.local of ur project on messenger

# Please get attention !
> No verification was check on python deemix at this time. What wanted to say is actually if you haven't installed deemix library globally, didn't iniate ARL before or another else configuration who could block download process. Any error will not be occured from deemix manager gui  
> Please keep in mind to test deemix manually before request why isn't working
