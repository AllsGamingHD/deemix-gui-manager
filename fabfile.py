from fabric.api import settings, local
from fabric.context_managers import lcd, shell_env

def start():
    local('symfony serve --no-tls -d')
    local('php bin/console messenger:consume async -vv')

def reset_db():
    local('php bin/console doctrine:schema:drop --force')
    local('php bin/console doctrine:schema:update --force')
    local('php bin/console doctrine:fixtures:load -n')

def translation():
    local('php bin/console translation:pull --force')

def composer_install():
    local('composer install')

def install():
    composer_install()
    reset_db()
    translation()
    start()